with x; use x;
with jni; use jni;

package Pizza_Test is
	function New_Pizza(E: JNIEnv; C: jclass) return jobject;
	pragma Export(Dll, New_Pizza, "Java_javada_x_PizzaTest_newPizza");
end Pizza_Test;