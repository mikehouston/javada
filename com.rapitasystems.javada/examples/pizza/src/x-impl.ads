with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package x.impl is
   -- Types for A_Topping
   type A_Topping is new Topping with private;

   -- Types for A_Toppings
   type A_Toppings is new Toppings with private;

   -- Types for A_Pizza
   type A_Pizza is new Pizza with private;

   -- Interface Methods for A_Topping
   function Hash_Code(Obj: A_Topping) return Hash_Type;
   function Equals (Obj1: A_Topping; Obj2: Topping'Class) return Boolean;
   function Is_Null(Obj: A_Topping) return Boolean;

   function GetName
     (Obj: A_Topping) return String;

   -- Interface Methods for A_Toppings
   function Hash_Code(Obj: A_Toppings) return Hash_Type;
   function Equals (Obj1: A_Toppings; Obj2: Toppings'Class) return Boolean;
   function Is_Null(Obj: A_Toppings) return Boolean;

   function Size
     (Obj: A_Toppings) return Natural;
   function Get
     (Obj: A_Toppings;
      Index: Natural) return Topping'Class;

   -- Interface Methods for A_Pizza
   function Hash_Code(Obj: A_Pizza) return Hash_Type;
   function Equals (Obj1: A_Pizza; Obj2: Pizza'Class) return Boolean;
   function Is_Null(Obj: A_Pizza) return Boolean;

   function GetToppings
     (Obj: A_Pizza) return Toppings'Class;
	
   function New_Pizza return A_Pizza;
	
private
	-- Types for A_Topping
   type A_Topping is new Topping with
    record
		Name: Unbounded_String;
	end record;

   -- Types for A_Toppings
   type T_array is array (0..1) of A_Topping;
   type A_Toppings is new Toppings with
    record
		Toppings: T_array;
	end record;

   -- Types for A_Pizza
   type A_Pizza is new Pizza with
    record
		T: A_Toppings;
	end record;

end x.impl;