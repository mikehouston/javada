package body x.impl is
   -- Interface Methods for A_Topping
   function Hash_Code(Obj: A_Topping) return Hash_Type is
   begin
      return Hash_Type(0);
   end;

   function Equals (Obj1: A_Topping; Obj2: Topping'Class) return Boolean is
   begin
      return False;
   end;

   function Is_Null(Obj: A_Topping) return Boolean is
   begin
      return False;
   end;

   function GetName
     (Obj: A_Topping) return String is
   begin
      return To_String(Obj.Name);
   end;


   -- Interface Methods for A_Toppings
   function Hash_Code(Obj: A_Toppings) return Hash_Type is
   begin
      return Hash_Type(0);
   end;

   function Equals (Obj1: A_Toppings; Obj2: Toppings'Class) return Boolean is
   begin
      return False;
   end;

   function Is_Null(Obj: A_Toppings) return Boolean is
   begin
      return False;
   end;


   function Size
     (Obj: A_Toppings) return Natural is
   begin
      return Obj.Toppings'Length;
   end;

   function Get
     (Obj: A_Toppings;
      Index: Natural) return Topping'Class is
   begin
      return Topping'Class(Obj.Toppings(Index));
   end;

   -- Interface Methods for A_Pizza
   function Hash_Code(Obj: A_Pizza) return Hash_Type is
   begin
      return Hash_Type(0);
   end;

   function Equals (Obj1: A_Pizza; Obj2: Pizza'Class) return Boolean is
   begin
      return False;
   end;

   function Is_Null(Obj: A_Pizza) return Boolean is
   begin
      return False;
   end;


   function GetToppings
     (Obj: A_Pizza) return Toppings'Class is
   begin
      return Toppings'Class(Obj.T);
   end;

   function New_Pizza return A_Pizza is
      Pizza: A_Pizza;
   begin
      Pizza.T.Toppings(0).Name := To_Unbounded_String("Pepperoni");
	  Pizza.T.Toppings(1).Name := To_Unbounded_String("Olives");
	  return Pizza;
   end;

end x.impl;