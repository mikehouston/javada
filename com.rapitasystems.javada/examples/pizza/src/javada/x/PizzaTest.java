package javada.x;

public class PizzaTest {
	public static native Pizza newPizza();
	
	public static void main(String[] args) {
		System.out.println("Testing pizza Javada interface");
		System.loadLibrary("pizza");
		
		Pizza p = newPizza();
		Toppings t = p.getToppings();
		System.out.println("Pizza has " + t.size() + " toppings:");
		for (int i = 0; i < t.size(); i++) {
			System.out.println("  " + t.get(i).getName());
		}
		
		System.out.println("\nDone");
	}
}