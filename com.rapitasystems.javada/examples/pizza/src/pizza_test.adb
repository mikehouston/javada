with x.Util; use x.Util;
with x.impl; use x.impl;

package body Pizza_Test is
	function New_Pizza(E: JNIEnv; C: jclass) return jobject is
		P: A_Pizza := New_Pizza;
	begin
		return Pizza_To_Java(E, Pizza'Class(P));
	end;
end Pizza_Test;
