with Interfaces.C;
with Interfaces.C.Strings;
with System;
with Ada.Tags; use Ada.Tags;
with Ada.Tags.Generic_Dispatching_Constructor;
with Ada.Exceptions; use Ada.Exceptions;

package jni is
   package C renames Interfaces.C;
   package CS renames Interfaces.C.Strings;
--/*
 -- * @(#)jni.h 1.56 03/12/19
 -- *
 -- * Copyright 2004 Sun Microsystems, Inc. All rights reserved.
 -- * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 -- */

-- /*
 -- * We used part of Netscape's Java Runtime Interface (JRI) as the starting
 -- * point of our design and implementation.
 -- */

-- /******************************************************************************
 -- * Java Runtime Interface
 -- * Copyright (c) 1996 Netscape Communications Corporation. All rights reserved.
 -- *****************************************************************************/

----

-- /* jni_md.h contains the machine-dependent typedefs for jbyte, jint
   -- and jlong */

   -- #include "jni_md.h"
--     /*
--   * @(#)jni_md.h	1.14 03/12/19
--   *
--   * Copyright 2004 Sun Microsystems, Inc. All rights reserved.
--   * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
--   */
--
----
--
   type jint is new C.long;
   --  typedef long jint;

   type int64 is
      range -(2 ** (64 - 1)) ..
            +(2 ** (64 - 1) - 1);

   type jlong is new int64;
   --  typedef __int64 jlong;
   type jbyte is new C.signed_char;
--  typedef signed char jbyte;
--
----

-- /*
 -- * JNI Types
 -- */

-- #ifndef JNI_TYPES_ALREADY_DEFINED_IN_JNI_MD_H

   type jboolean is new C.unsigned_char;
   type jboolean_ptr is access jboolean;
   -- typedef unsigned char     jboolean;
   type jchar is new C.unsigned_short;
   -- typedef unsigned short       jchar;
   type jshort is new C.short;
   -- typedef short                jshort;
   type jfloat is new C.C_float;
   -- typedef float                jfloat;
   type jdouble is new C.double;
   -- typedef double               jdouble;

   type jsize is new jint;
   -- typedef jint            jsize;

-- <C++ defs removed>

--     type _jobject is private;
--     -- struct _jobject;

   --     type jobject is access _jobject;
   subtype Jobject is System.Address;

   -- typedef struct _jobject *jobject;
   subtype jclass is jobject;
   -- typedef jobject jclass;
   subtype jthrowable is jobject;
   -- typedef jobject jthrowable;
   subtype jstring is jobject;
   -- typedef jobject jstring;
   subtype jarray is jobject;
   -- typedef jobject jarray;
   subtype jbooleanArray is jarray;
   -- typedef jarray jbooleanArray;
   subtype jbyteArray is jarray;
   -- typedef jarray jbyteArray;
   subtype jcharArray is jarray;
   -- typedef jarray jcharArray;
   subtype jshortArray is jarray;
   -- typedef jarray jshortArray;
   subtype jintArray is jarray;
   -- typedef jarray jintArray;
   subtype jlongArray is jarray;
   -- typedef jarray jlongArray;
   subtype jfloatArray is jarray;
   -- typedef jarray jfloatArray;
   subtype jdoubleArray is jarray;
   -- typedef jarray jdoubleArray;
   subtype jobjectArray is jarray;
   -- typedef jarray jobjectArray;

-- #endif

   subtype jweak is jobject;
   -- typedef jobject jweak;

   type Jtypes is (V_Boolean, V_Byte, V_Char, V_Short, V_Int, V_Long, V_Float, V_Double, V_Object);
   type jvalue(kind : jtypes := V_object) is
      record
         case kind is
            when V_boolean =>
               z: jboolean;
            when V_byte =>
               b: jbyte;
            when V_char =>
               c: jchar;
            when V_short =>
               s: jshort;
            when V_int =>
               i: jint;
            when V_long =>
               j: jlong;
            when V_float =>
               f: jfloat;
            when V_double =>
               d: jdouble;
            when V_object =>
               l: jobject;
         end case;
      end record;
   pragma Unchecked_Union(Jvalue);

   type jvalue_Array is array (Integer range <>) of aliased jvalue;
   --pragma Pack(jvalue_Array);

   -- typedef union jvalue {
    -- jboolean z;
    -- jbyte    b;
    -- jchar    c;
    -- jshort   s;
    -- jint     i;
    -- jlong    j;
    -- jfloat   f;
    -- jdouble  d;
    -- jobject  l;
    -- } jvalue;

--     type _jfieldID is private;
   -- struct _jfieldID;
--     type jfieldID is access _jfieldID;
   subtype jfieldID is System.Address;
   -- typedef struct _jfieldID *jfieldID;

--     type _jmethodID is private;
   -- struct _jmethodID;
--     type jmethodID is access _jmethodID;
   subtype jmethodID is System.Address;
   -- typedef struct _jmethodID *jmethodID;

-- #endif /* JNI_TYPES_ALREADY_DEFINED_IN_JNI_MD_H */

-- /*
 -- * jboolean constants
 -- */

   JNI_FALSE : constant jboolean := 0;
   -- #define JNI_FALSE 0
   JNI_TRUE : constant jboolean := 1;
-- #define JNI_TRUE 1

-- /*
 -- * possible return values for JNI functions.
 -- */

   type JNI_Return_Value is
     (JNI_EINVAL,
      JNI_EEXIST,
      JNI_ENOMEM,
      JNI_EVERSION,
      JNI_EDETACHED,
      JNI_ERR,
      JNI_OK);
   for JNI_Return_Value use
     (JNI_OK => 0,
      JNI_ERR => -1,
      JNI_EDETACHED => -2,
      JNI_EVERSION => -3,
      JNI_ENOMEM => -4,
      JNI_EEXIST => -5,
      JNI_EINVAL => -6);
-- #define JNI_OK           0                 /* success */
-- #define JNI_ERR          (-1)              /* unknown error */
-- #define JNI_EDETACHED    (-2)              /* thread detached from the VM */
-- #define JNI_EVERSION     (-3)              /* JNI version error */
-- #define JNI_ENOMEM       (-4)              /* not enough memory */
-- #define JNI_EEXIST       (-5)              /* VM already created */
-- #define JNI_EINVAL       (-6)              /* invalid arguments */

-- /*
 -- * used in ReleaseScalarArrayElements
 -- */

   type Release_Value is
     (JNI_COMMIT,
      JNI_ABORT);
   for Release_Value use
     (JNI_COMMIT => 1,
      JNI_ABORT => 2);
-- #define JNI_COMMIT 1
-- #define JNI_ABORT 2

-- /*
 -- * used in RegisterNatives to describe native method name, signature,
 -- * and function pointer.
 -- */

   type JNINativeMethod is
      record
         name : access C.Char;
         signature : access C.Char;
         fnPtr : access procedure;
         -- FIXME: only allows procedures with no parameters
         -- Could create new System.Pointers package?
      end record;
-- typedef struct {
    -- char *name;
    -- char *signature;
    -- void *fnPtr;
-- } JNINativeMethod;

-- /*
 -- * JNI Native Method Interface.
 -- */

   type t_JNINativeInterface;
   type t_JNINativeInterface_ptr is access t_JNINativeInterface;
   -- struct JNINativeInterface_;

   -- <removed C++ specific stuff>

   type JNIEnv is access t_JNINativeInterface_ptr;
   -- typedef const struct JNINativeInterface_ *JNIEnv;

-- /*
 -- * JNI Invocation Interface.
 -- */

   type t_JNIInvokeInterface;
   type t_JNIInvokeInterface_ptr is access t_JNIInvokeInterface;
   -- struct JNIInvokeInterface_;

   -- <removed C++ stuff>
   type JavaVM is access t_JNIInvokeInterface_ptr;
   -- typedef const struct JNIInvokeInterface_ *JavaVM;

   -- Function prototypes

   type voidFunction is access procedure;

   -------------------------------------------------------
   -- JNI function prototypes
   -------------------------------------------------------

      -- struct JNINativeInterface_ {
         type F_reserved0  is new voidFunction;
         pragma Convention(StdCall, F_reserved0 );
      -- void *reserved0;
         type F_reserved1  is new voidFunction;
         pragma Convention(StdCall, F_reserved1 );
      -- void *reserved1;
         type F_reserved2  is new voidFunction;
         pragma Convention(StdCall, F_reserved2 );
      -- void *reserved2;
         type F_reserved3  is new voidFunction;
         pragma Convention(StdCall, F_reserved3 );
         -- void *reserved3;

         type F_GetVersion  is access function (E: JNIEnv) return jint;
         pragma Convention(StdCall, F_GetVersion );
      -- jint (JNICALL *GetVersion)(JNIEnv *env);

         type F_DefineClass  is new voidFunction;
         pragma Convention(StdCall, F_DefineClass );
    -- jclass (JNICALL *DefineClass)
      -- (JNIEnv *env, const char *name, jobject loader, const jbyte *buf,
    -- jsize len);
         type F_FindClass  is access function (env : JNIEnv; name : access C.char) return jclass;
         pragma Convention(StdCall, F_FindClass );
    -- jclass (JNICALL *FindClass)
      -- (JNIEnv *env, const char *name);

         type F_FromReflectedMethod  is access function (env : JNIEnv; method: jobject) return jmethodID;
         pragma Convention(StdCall, F_FromReflectedMethod );
    -- jmethodID (JNICALL *FromReflectedMethod)
    -- (JNIEnv *env, jobject method);
         type F_FromReflectedField  is access function (env : JNIEnv; field: jobject) return jfieldID;
         pragma Convention(StdCall, F_FromReflectedField );
    -- jfieldID (JNICALL *FromReflectedField)
      -- (JNIEnv *env, jobject field);

         type F_ToReflectedMethod  is access function(E: JNIEnv; cls: jclass; methodID: jmethodID; isStatic: jboolean) return jobject;
         pragma Convention(StdCall, F_ToReflectedMethod );
    -- jobject (JNICALL *ToReflectedMethod)
      -- (JNIEnv *env, jclass cls, jmethodID methodID, jboolean isStatic);

         type F_GetSuperclass is access function(E: JNIEnv; sub: jclass) return jclass;
         pragma Convention(StdCall, F_GetSuperclass);
    -- jclass (JNICALL *GetSuperclass)
    -- (JNIEnv *env, jclass sub);
         type F_IsAssignableFrom is access function(E: JNIEnv; sub, sup: jclass) return jboolean;
         pragma Convention(StdCall, F_IsAssignableFrom);
    -- jboolean (JNICALL *IsAssignableFrom)
      -- (JNIEnv *env, jclass sub, jclass sup);

         type F_ToReflectedField is access function(E: JNIEnv; cls: jclass; fieldID: jfieldID; isStatic: jboolean) return jobject;
         pragma Convention(StdCall, F_ToReflectedField);
    -- jobject (JNICALL *ToReflectedField)
      -- (JNIEnv *env, jclass cls, jfieldID fieldID, jboolean isStatic);

         type F_Throw is access function(E: JNIEnv; Obj: jthrowable) return jint;
         pragma Convention(StdCall, F_Throw);
    -- jint (JNICALL *Throw)
    -- (JNIEnv *env, jthrowable obj);
         type F_ThrowNew is access function(E: JNIEnv; clazz: jclass; msg: access C.Char) return jint;
         pragma Convention(StdCall, F_ThrowNew);
    -- jint (JNICALL *ThrowNew)
    -- (JNIEnv *env, jclass clazz, const char *msg);
         type F_ExceptionOccurred is access function(E: JNIEnv) return jthrowable;
         pragma Convention(StdCall, F_ExceptionOccurred);
    -- jthrowable (JNICALL *ExceptionOccurred)
    -- (JNIEnv *env);
         type F_ExceptionDescribe is access procedure(E: JNIEnv);
         pragma Convention(StdCall, F_ExceptionDescribe);
    -- void (JNICALL *ExceptionDescribe)
    -- (JNIEnv *env);
         type F_ExceptionClear is access procedure(E: JNIEnv);
         pragma Convention(StdCall, F_ExceptionClear);
    -- void (JNICALL *ExceptionClear)
    -- (JNIEnv *env);
         type F_FatalError is access procedure(E: JNIEnv; msg: access C.Char);
         pragma Convention(StdCall, F_FatalError);
    -- void (JNICALL *FatalError)
      -- (JNIEnv *env, const char *msg);

         type F_PushLocalFrame is new voidFunction;
         pragma Convention(StdCall, F_PushLocalFrame);
    -- jint (JNICALL *PushLocalFrame)
    -- (JNIEnv *env, jint capacity);
         type F_PopLocalFrame is new voidFunction;
         pragma Convention(StdCall, F_PopLocalFrame);
    -- jobject (JNICALL *PopLocalFrame)
      -- (JNIEnv *env, jobject result);

         type F_NewGlobalRef is access function(E: JNIEnv; O: jobject) return jobject;
         pragma Convention(StdCall, F_NewGlobalRef);
    -- jobject (JNICALL *NewGlobalRef)
    -- (JNIEnv *env, jobject lobj);
         type F_DeleteGlobalRef is access procedure(E: JNIEnv; O: jobject);
         pragma Convention(StdCall, F_DeleteGlobalRef);
    -- void (JNICALL *DeleteGlobalRef)
    -- (JNIEnv *env, jobject gref);
         type F_DeleteLocalRef is access procedure(E: JNIEnv; O: jobject);
         pragma Convention(StdCall, F_DeleteLocalRef);
    -- void (JNICALL *DeleteLocalRef)
    -- (JNIEnv *env, jobject obj);
         type F_IsSameObject is access function(E: JNIEnv; obj1, obj2: jobject) return jboolean;
         pragma Convention(StdCall, F_IsSameObject);
    -- jboolean (JNICALL *IsSameObject)
    -- (JNIEnv *env, jobject obj1, jobject obj2);
         type F_NewLocalRef is new voidFunction;
         pragma Convention(StdCall, F_NewLocalRef);
    -- jobject (JNICALL *NewLocalRef)
    -- (JNIEnv *env, jobject ref);
         type F_EnsureLocalCapacity is new voidFunction;
         pragma Convention(StdCall, F_EnsureLocalCapacity);
    -- jint (JNICALL *EnsureLocalCapacity)
      -- (JNIEnv *env, jint capacity);

         type F_AllocObject is access function(E: JNIEnv; clazz: jclass) return jobject;
         pragma Convention(StdCall, F_AllocObject);
    -- jobject (JNICALL *AllocObject)
    -- (JNIEnv *env, jclass clazz);
         type F_NewObject is new voidFunction;
         pragma Convention(StdCall, F_NewObject);
    -- jobject (JNICALL *NewObject)
    -- (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_NewObjectV is new voidFunction;
         pragma Convention(StdCall, F_NewObjectV);
    -- jobject (JNICALL *NewObjectV)
    -- (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_NewObjectA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jobject;
         pragma Convention(StdCall, F_NewObjectA);
    -- jobject (JNICALL *NewObjectA)
      -- (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);

         type F_GetObjectClass is access function (E: JNIEnv; obj: jobject) return jclass;
         pragma Convention(StdCall, F_GetObjectClass);
    -- jclass (JNICALL *GetObjectClass)
    -- (JNIEnv *env, jobject obj);
         type F_IsInstanceOf is access function (E: JNIEnv; obj: jobject; clazz: jclass) return jboolean;
         pragma Convention(StdCall, F_IsInstanceOf);
    -- jboolean (JNICALL *IsInstanceOf)
      -- (JNIEnv *env, jobject obj, jclass clazz);

         type F_GetMethodID  is access function(E: JNIEnv; clazz : jclass; name, sig : access C.Char) return jmethodID;
         pragma Convention(StdCall, F_GetMethodID );
    -- jmethodID (JNICALL *GetMethodID)
      -- (JNIEnv *env, jclass clazz, const char *name, const char *sig);

         type F_CallObjectMethod is new voidFunction;
         pragma Convention(StdCall, F_CallObjectMethod);
    -- jobject (JNICALL *CallObjectMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallObjectMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallObjectMethodV);
    -- jobject (JNICALL *CallObjectMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallObjectMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jobject;
         pragma Convention(StdCall, F_CallObjectMethodA);
    -- jobject (JNICALL *CallObjectMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);

         type F_CallBooleanMethod is new voidFunction;
         pragma Convention(StdCall, F_CallBooleanMethod);
    -- jboolean (JNICALL *CallBooleanMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallBooleanMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallBooleanMethodV);
    -- jboolean (JNICALL *CallBooleanMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallBooleanMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jboolean;
         pragma Convention(StdCall, F_CallBooleanMethodA);
    -- jboolean (JNICALL *CallBooleanMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);

         type F_CallByteMethod is new voidFunction;
         pragma Convention(StdCall, F_CallByteMethod);
    -- jbyte (JNICALL *CallByteMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallByteMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallByteMethodV);
    -- jbyte (JNICALL *CallByteMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallByteMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jbyte;
         pragma Convention(StdCall, F_CallByteMethodA);
    -- jbyte (JNICALL *CallByteMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         type F_CallCharMethod is new voidFunction;
         pragma Convention(StdCall, F_CallCharMethod);
    -- jchar (JNICALL *CallCharMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallCharMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallCharMethodV);
    -- jchar (JNICALL *CallCharMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallCharMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jchar;
         pragma Convention(StdCall, F_CallCharMethodA);
    -- jchar (JNICALL *CallCharMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         type F_CallShortMethod is new voidFunction;
         pragma Convention(StdCall, F_CallShortMethod);
    -- jshort (JNICALL *CallShortMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallShortMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallShortMethodV);
    -- jshort (JNICALL *CallShortMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallShortMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jshort;
         pragma Convention(StdCall, F_CallShortMethodA);
    -- jshort (JNICALL *CallShortMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         type F_CallIntMethod is new voidFunction;
         pragma Convention(StdCall, F_CallIntMethod);
    -- jint (JNICALL *CallIntMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallIntMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallIntMethodV);
    -- jint (JNICALL *CallIntMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallIntMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jint;
         pragma Convention(StdCall, F_CallIntMethodA);
    -- jint (JNICALL *CallIntMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         type F_CallLongMethod is new voidFunction;
         pragma Convention(StdCall, F_CallLongMethod);
    -- jlong (JNICALL *CallLongMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallLongMethodV is new voidFunction;
         pragma Convention(StdCall, F_CallLongMethodV);
    -- jlong (JNICALL *CallLongMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallLongMethodA is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jlong;
         pragma Convention(StdCall, F_CallLongMethodA);
    -- jlong (JNICALL *CallLongMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         type F_CallFloatMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallFloatMethod );
      --    jfloat (JNICALL *CallFloatMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallFloatMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallFloatMethodV );
      --    jfloat (JNICALL *CallFloatMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallFloatMethodA  is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jfloat;
         pragma Convention(StdCall, F_CallFloatMethodA );
      --    jfloat (JNICALL *CallFloatMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);
      --
         type F_CallDoubleMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallDoubleMethod );
      --    jdouble (JNICALL *CallDoubleMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallDoubleMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallDoubleMethodV );
      --    jdouble (JNICALL *CallDoubleMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallDoubleMethodA  is access function (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue) return jdouble;
         pragma Convention(StdCall, F_CallDoubleMethodA );
      --    jdouble (JNICALL *CallDoubleMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);
      --
         type F_CallVoidMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallVoidMethod );
      --    void (JNICALL *CallVoidMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         type F_CallVoidMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallVoidMethodV );
      --    void (JNICALL *CallVoidMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         type F_CallVoidMethodA  is access procedure (E: JNIEnv; obj: jobject; methodID: jmethodID; args: access jvalue);
         pragma Convention(StdCall, F_CallVoidMethodA );
      --    void (JNICALL *CallVoidMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);
      --
         type F_CallNonvirtualObjectMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualObjectMethod );
      --    jobject (JNICALL *CallNonvirtualObjectMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualObjectMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualObjectMethodV );
      --    jobject (JNICALL *CallNonvirtualObjectMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualObjectMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jobject;
         pragma Convention(StdCall, F_CallNonvirtualObjectMethodA);
      --    jobject (JNICALL *CallNonvirtualObjectMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
         type F_CallNonvirtualBooleanMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualBooleanMethod );
      --    jboolean (JNICALL *CallNonvirtualBooleanMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualBooleanMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualBooleanMethodV );
      --    jboolean (JNICALL *CallNonvirtualBooleanMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualBooleanMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jboolean;
         pragma Convention(StdCall, F_CallNonvirtualBooleanMethodA);
      --    jboolean (JNICALL *CallNonvirtualBooleanMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
         type F_CallNonvirtualByteMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualByteMethod );
      --    jbyte (JNICALL *CallNonvirtualByteMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualByteMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualByteMethodV );
      --    jbyte (JNICALL *CallNonvirtualByteMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualByteMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jbyte;
         pragma Convention(StdCall, F_CallNonvirtualByteMethodA);
      --    jbyte (JNICALL *CallNonvirtualByteMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualCharMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualCharMethod );
      --    jchar (JNICALL *CallNonvirtualCharMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualCharMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualCharMethodV );
      --    jchar (JNICALL *CallNonvirtualCharMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualCharMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jchar;
         pragma Convention(StdCall, F_CallNonvirtualCharMethodA);
      --    jchar (JNICALL *CallNonvirtualCharMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualShortMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualShortMethod );
      --    jshort (JNICALL *CallNonvirtualShortMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualShortMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualShortMethodV );
      --    jshort (JNICALL *CallNonvirtualShortMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualShortMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jshort;
         pragma Convention(StdCall, F_CallNonvirtualShortMethodA);
      --    jshort (JNICALL *CallNonvirtualShortMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualIntMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualIntMethod );
      --    jint (JNICALL *CallNonvirtualIntMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualIntMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualIntMethodV );
      --    jint (JNICALL *CallNonvirtualIntMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualIntMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jint;
         pragma Convention(StdCall, F_CallNonvirtualIntMethodA);
      --    jint (JNICALL *CallNonvirtualIntMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualLongMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualLongMethod );
      --    jlong (JNICALL *CallNonvirtualLongMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualLongMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualLongMethodV );
      --    jlong (JNICALL *CallNonvirtualLongMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualLongMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jlong;
         pragma Convention(StdCall, F_CallNonvirtualLongMethodA);
      --    jlong (JNICALL *CallNonvirtualLongMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualFloatMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualFloatMethod );
      --    jfloat (JNICALL *CallNonvirtualFloatMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualFloatMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualFloatMethodV );
      --    jfloat (JNICALL *CallNonvirtualFloatMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualFloatMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jfloat;
         pragma Convention(StdCall, F_CallNonvirtualFloatMethodA);
      --    jfloat (JNICALL *CallNonvirtualFloatMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualDoubleMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualDoubleMethod );
      --    jdouble (JNICALL *CallNonvirtualDoubleMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualDoubleMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualDoubleMethodV );
      --    jdouble (JNICALL *CallNonvirtualDoubleMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualDoubleMethodA is access function(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue) return jdouble;
         pragma Convention(StdCall, F_CallNonvirtualDoubleMethodA);
      --    jdouble (JNICALL *CallNonvirtualDoubleMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         type F_CallNonvirtualVoidMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualVoidMethod );
      --    void (JNICALL *CallNonvirtualVoidMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         type F_CallNonvirtualVoidMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallNonvirtualVoidMethodV );
      --    void (JNICALL *CallNonvirtualVoidMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         type F_CallNonvirtualVoidMethodA is access procedure(E: JNIEnv; obj: jobject; clazz: jclass; methodID: jmethodID; args: access jvalue);
         pragma Convention(StdCall, F_CallNonvirtualVoidMethodA);
      --    void (JNICALL *CallNonvirtualVoidMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
		type F_GetFieldID is access function (E: JNIEnv; Clazz: Jclass; Name, Sig: access C.Char) return JfieldID;
		pragma Convention(StdCall, F_GetFieldID);
      --    jfieldID (JNICALL *GetFieldID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
      --
         type F_GetObjectField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jobject;
         pragma Convention(StdCall, F_GetObjectField);
      --    jobject (JNICALL *GetObjectField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetBooleanField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jboolean;
         pragma Convention(StdCall, F_GetBooleanField);
      --    jboolean (JNICALL *GetBooleanField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetByteField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jbyte;
         pragma Convention(StdCall, F_GetByteField);
      --    jbyte (JNICALL *GetByteField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetCharField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jchar;
         pragma Convention(StdCall, F_GetCharField);
      --    jchar (JNICALL *GetCharField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetShortField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jshort;
         pragma Convention(StdCall, F_GetShortField);
      --    jshort (JNICALL *GetShortField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetIntField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jint;
         pragma Convention(StdCall, F_GetIntField);
      --    jint (JNICALL *GetIntField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetLongField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jlong;
         pragma Convention(StdCall, F_GetLongField);
      --    jlong (JNICALL *GetLongField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetFloatField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jfloat;
         pragma Convention(StdCall, F_GetFloatField);
      --    jfloat (JNICALL *GetFloatField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         type F_GetDoubleField is access function (E: JNIEnv; obj: jobject; fieldID: jfieldID) return jdouble;
         pragma Convention(StdCall, F_GetDoubleField);
      --    jdouble (JNICALL *GetDoubleField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
      --
         type F_SetObjectField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jobject);
         pragma Convention(StdCall, F_SetObjectField);
      --    void (JNICALL *SetObjectField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jobject val);
         type F_SetBooleanField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jboolean);
         pragma Convention(StdCall, F_SetBooleanField);
      --    void (JNICALL *SetBooleanField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jboolean val);
         type F_SetByteField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jbyte);
         pragma Convention(StdCall, F_SetByteField);
      --    void (JNICALL *SetByteField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jbyte val);
         type F_SetCharField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jchar);
         pragma Convention(StdCall, F_SetCharField);
      --    void (JNICALL *SetCharField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jchar val);
         type F_SetShortField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jshort);
         pragma Convention(StdCall, F_SetShortField);
      --    void (JNICALL *SetShortField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jshort val);
         type F_SetIntField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jint);
         pragma Convention(StdCall, F_SetIntField);
      --    void (JNICALL *SetIntField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jint val);
         type F_SetLongField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jlong);
         pragma Convention(StdCall, F_SetLongField);
      --    void (JNICALL *SetLongField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jlong val);
         type F_SetFloatField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jfloat);
         pragma Convention(StdCall, F_SetFloatField);
      --    void (JNICALL *SetFloatField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jfloat val);
         type F_SetDoubleField is access procedure (E: JNIEnv; obj: jobject; fieldID: jfieldID; val: jdouble);
         pragma Convention(StdCall, F_SetDoubleField);
      --    void (JNICALL *SetDoubleField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jdouble val);
      --
         type F_GetStaticMethodID is access function (E: JNIEnv; clazz: jclass; name, sig: access C.Char) return jmethodID;
         pragma Convention(StdCall, F_GetStaticMethodID);
      --    jmethodID (JNICALL *GetStaticMethodID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
      --
         type F_CallStaticObjectMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticObjectMethod );
      --    jobject (JNICALL *CallStaticObjectMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticObjectMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticObjectMethodV );
      --    jobject (JNICALL *CallStaticObjectMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticObjectMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jobject;
         pragma Convention(StdCall, F_CallStaticObjectMethodA);
      --    jobject (JNICALL *CallStaticObjectMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticBooleanMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticBooleanMethod );
      --    jboolean (JNICALL *CallStaticBooleanMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticBooleanMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticBooleanMethodV );
      --    jboolean (JNICALL *CallStaticBooleanMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticBooleanMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jboolean;
         pragma Convention(StdCall, F_CallStaticBooleanMethodA);
      --    jboolean (JNICALL *CallStaticBooleanMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticByteMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticByteMethod );
      --    jbyte (JNICALL *CallStaticByteMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticByteMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticByteMethodV );
      --    jbyte (JNICALL *CallStaticByteMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticByteMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jbyte;
         pragma Convention(StdCall, F_CallStaticByteMethodA);
      --    jbyte (JNICALL *CallStaticByteMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticCharMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticCharMethod );
      --    jchar (JNICALL *CallStaticCharMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticCharMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticCharMethodV );
      --    jchar (JNICALL *CallStaticCharMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticCharMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jchar;
         pragma Convention(StdCall, F_CallStaticCharMethodA);
      --    jchar (JNICALL *CallStaticCharMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticShortMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticShortMethod );
      --    jshort (JNICALL *CallStaticShortMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticShortMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticShortMethodV );
      --    jshort (JNICALL *CallStaticShortMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticShortMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jshort;
         pragma Convention(StdCall, F_CallStaticShortMethodA);
      --    jshort (JNICALL *CallStaticShortMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticIntMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticIntMethod );
      --    jint (JNICALL *CallStaticIntMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticIntMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticIntMethodV );
      --    jint (JNICALL *CallStaticIntMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticIntMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jint;
         pragma Convention(StdCall, F_CallStaticIntMethodA);
      --    jint (JNICALL *CallStaticIntMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticLongMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticLongMethod );
      --    jlong (JNICALL *CallStaticLongMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticLongMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticLongMethodV );
      --    jlong (JNICALL *CallStaticLongMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticLongMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jlong;
         pragma Convention(StdCall, F_CallStaticLongMethodA);
      --    jlong (JNICALL *CallStaticLongMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticFloatMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticFloatMethod );
      --    jfloat (JNICALL *CallStaticFloatMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticFloatMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticFloatMethodV );
      --    jfloat (JNICALL *CallStaticFloatMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticFloatMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jfloat;
         pragma Convention(StdCall, F_CallStaticFloatMethodA);
      --    jfloat (JNICALL *CallStaticFloatMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticDoubleMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticDoubleMethod );
      --    jdouble (JNICALL *CallStaticDoubleMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         type F_CallStaticDoubleMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticDoubleMethodV );
      --    jdouble (JNICALL *CallStaticDoubleMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         type F_CallStaticDoubleMethodA is access function(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue) return jdouble;
         pragma Convention(StdCall, F_CallStaticDoubleMethodA);
      --    jdouble (JNICALL *CallStaticDoubleMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         type F_CallStaticVoidMethod  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticVoidMethod );
      --    void (JNICALL *CallStaticVoidMethod)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, ...);
         type F_CallStaticVoidMethodV  is new voidFunction;
         pragma Convention(StdCall, F_CallStaticVoidMethodV );
      --    void (JNICALL *CallStaticVoidMethodV)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, va_list args);
         type F_CallStaticVoidMethodA is access procedure(E: JNIEnv; clazz: jclass; methodID: jmethodID; args: access jvalue);
         pragma Convention(StdCall, F_CallStaticVoidMethodA);
      --    void (JNICALL *CallStaticVoidMethodA)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, const jvalue * args);
      --

         type F_GetStaticFieldID is access function (E: JNIEnv; clazz: jclass; name, sig: access C.Char) return jfieldID;
         pragma Convention(StdCall, F_GetStaticFieldID);
      --    jfieldID (JNICALL *GetStaticFieldID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
         type F_GetStaticObjectField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jobject;
         pragma Convention(StdCall, F_GetStaticObjectField);
      --    jobject (JNICALL *GetStaticObjectField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticBooleanField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jboolean;
         pragma Convention(StdCall, F_GetStaticBooleanField);
      --    jboolean (JNICALL *GetStaticBooleanField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticByteField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jbyte;
         pragma Convention(StdCall, F_GetStaticByteField);
      --    jbyte (JNICALL *GetStaticByteField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticCharField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jchar;
         pragma Convention(StdCall, F_GetStaticCharField);
      --    jchar (JNICALL *GetStaticCharField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticShortField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jshort;
         pragma Convention(StdCall, F_GetStaticShortField);
      --    jshort (JNICALL *GetStaticShortField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticIntField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jint;
         pragma Convention(StdCall, F_GetStaticIntField);
      --    jint (JNICALL *GetStaticIntField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticLongField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jlong;
         pragma Convention(StdCall, F_GetStaticLongField);
      --    jlong (JNICALL *GetStaticLongField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticFloatField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jfloat;
         pragma Convention(StdCall, F_GetStaticFloatField);
      --    jfloat (JNICALL *GetStaticFloatField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         type F_GetStaticDoubleField is access function (E: JNIEnv; clazz: jclass; fieldID: jfieldID) return jdouble;
         pragma Convention(StdCall, F_GetStaticDoubleField);
      --    jdouble (JNICALL *GetStaticDoubleField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
      --
         type F_SetStaticObjectField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jobject);
         pragma Convention(StdCall, F_SetStaticObjectField);
      --    void (JNICALL *SetStaticObjectField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jobject value);
         type F_SetStaticBooleanField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jboolean);
         pragma Convention(StdCall, F_SetStaticBooleanField);
      --    void (JNICALL *SetStaticBooleanField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jboolean value);
         type F_SetStaticByteField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jbyte);
         pragma Convention(StdCall, F_SetStaticByteField);
      --    void (JNICALL *SetStaticByteField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jbyte value);
         type F_SetStaticCharField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jchar);
         pragma Convention(StdCall, F_SetStaticCharField);
      --    void (JNICALL *SetStaticCharField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jchar value);
         type F_SetStaticShortField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jshort);
         pragma Convention(StdCall, F_SetStaticShortField);
      --    void (JNICALL *SetStaticShortField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jshort value);
         type F_SetStaticIntField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jint);
         pragma Convention(StdCall, F_SetStaticIntField);
      --    void (JNICALL *SetStaticIntField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jint value);
         type F_SetStaticLongField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jlong);
         pragma Convention(StdCall, F_SetStaticLongField);
      --    void (JNICALL *SetStaticLongField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jlong value);
         type F_SetStaticFloatField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jfloat);
         pragma Convention(StdCall, F_SetStaticFloatField);
      --    void (JNICALL *SetStaticFloatField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jfloat value);
         type F_SetStaticDoubleField is access procedure (E: JNIEnv; clazz: jclass; fieldID: jfieldID; val: jdouble);
         pragma Convention(StdCall, F_SetStaticDoubleField);
      --    void (JNICALL *SetStaticDoubleField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jdouble value);

         type F_NewString  is new voidFunction;
         pragma Convention(StdCall, F_NewString );
      --    jstring (JNICALL *NewString)
      --      (JNIEnv *env, const jchar *unicode, jsize len);
         type F_GetStringLength  is new voidFunction;
         pragma Convention(StdCall, F_GetStringLength );
      --    jsize (JNICALL *GetStringLength)
      --      (JNIEnv *env, jstring str);
         type F_GetStringChars  is new voidFunction;
         pragma Convention(StdCall, F_GetStringChars );
      --    const jchar *(JNICALL *GetStringChars)
      --      (JNIEnv *env, jstring str, jboolean *isCopy);
         type F_ReleaseStringChars  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseStringChars );
      --    void (JNICALL *ReleaseStringChars)
      --      (JNIEnv *env, jstring str, const jchar *chars);
      --
         type F_NewStringUTF  is access function (E: JNIEnv; utf: access C.Char) return jstring;
         pragma Convention(StdCall, F_NewStringUTF );
      --    jstring (JNICALL *NewStringUTF)
      --      (JNIEnv *env, const char *utf);
         type F_GetStringUTFLength  is access function (E: JNIEnv; S: jstring) return jsize;
         pragma Convention(StdCall, F_GetStringUTFLength );
      --    jsize (JNICALL *GetStringUTFLength)
      --      (JNIEnv *env, jstring str);
         type F_GetStringUTFChars  is access function (E: JNIEnv; S: jstring; isCopy : jboolean_ptr) return CS.Chars_Ptr;
         pragma Convention(StdCall, F_GetStringUTFChars );
      --    const char* (JNICALL *GetStringUTFChars)
      --      (JNIEnv *env, jstring str, jboolean *isCopy);
         type F_ReleaseStringUTFChars  is access procedure (E: JNIEnv; S: jstring; Chars: CS.Chars_Ptr);
         pragma Convention(StdCall, F_ReleaseStringUTFChars );
      --    void (JNICALL *ReleaseStringUTFChars)
      --      (JNIEnv *env, jstring str, const char* chars);
      --
      --
         type F_GetArrayLength  is access function (E: JNIEnv; A: jarray) return jsize;
         pragma Convention(StdCall, F_GetArrayLength );
      --    jsize (JNICALL *GetArrayLength)
      --      (JNIEnv *env, jarray array);
      --
         type F_NewObjectArray  is access function (E: JNIEnv; len: jsize; clazz: jclass; init: jobject) return jobjectArray;
         pragma Convention(StdCall, F_NewObjectArray );
      --    jobjectArray (JNICALL *NewObjectArray)
      --      (JNIEnv *env, jsize len, jclass clazz, jobject init);
         type F_GetObjectArrayElement  is access function (E: JNIEnv; A: jobjectArray; Index: jsize) return jobject;
         pragma Convention(StdCall, F_GetObjectArrayElement );
      --    jobject (JNICALL *GetObjectArrayElement)
      --      (JNIEnv *env, jobjectArray array, jsize index);
         type F_SetObjectArrayElement  is access procedure (E: JNIEnv; A: jobjectArray; Index: jsize; Val: jobject);
         pragma Convention(StdCall, F_SetObjectArrayElement );
      --    void (JNICALL *SetObjectArrayElement)
      --      (JNIEnv *env, jobjectArray array, jsize index, jobject val);
      --
         type F_NewBooleanArray  is new voidFunction;
         pragma Convention(StdCall, F_NewBooleanArray );
      --    jbooleanArray (JNICALL *NewBooleanArray)
      --      (JNIEnv *env, jsize len);
         type F_NewByteArray  is new voidFunction;
         pragma Convention(StdCall, F_NewByteArray );
      --    jbyteArray (JNICALL *NewByteArray)
      --      (JNIEnv *env, jsize len);
         type F_NewCharArray  is new voidFunction;
         pragma Convention(StdCall, F_NewCharArray );
      --    jcharArray (JNICALL *NewCharArray)
      --      (JNIEnv *env, jsize len);
         type F_NewShortArray  is new voidFunction;
         pragma Convention(StdCall, F_NewShortArray );
      --    jshortArray (JNICALL *NewShortArray)
      --      (JNIEnv *env, jsize len);
         type F_NewIntArray  is new voidFunction;
         pragma Convention(StdCall, F_NewIntArray );
      --    jintArray (JNICALL *NewIntArray)
      --      (JNIEnv *env, jsize len);
         type F_NewLongArray  is new voidFunction;
         pragma Convention(StdCall, F_NewLongArray );
      --    jlongArray (JNICALL *NewLongArray)
      --      (JNIEnv *env, jsize len);
         type F_NewFloatArray  is new voidFunction;
         pragma Convention(StdCall, F_NewFloatArray );
      --    jfloatArray (JNICALL *NewFloatArray)
      --      (JNIEnv *env, jsize len);
         type F_NewDoubleArray  is new voidFunction;
         pragma Convention(StdCall, F_NewDoubleArray );
      --    jdoubleArray (JNICALL *NewDoubleArray)
      --      (JNIEnv *env, jsize len);
      --
         type F_GetBooleanArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetBooleanArrayElements );
      --    jboolean * (JNICALL *GetBooleanArrayElements)
      --      (JNIEnv *env, jbooleanArray array, jboolean *isCopy);
         type F_GetByteArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetByteArrayElements );
      --    jbyte * (JNICALL *GetByteArrayElements)
      --      (JNIEnv *env, jbyteArray array, jboolean *isCopy);
         type F_GetCharArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetCharArrayElements );
      --    jchar * (JNICALL *GetCharArrayElements)
      --      (JNIEnv *env, jcharArray array, jboolean *isCopy);
         type F_GetShortArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetShortArrayElements );
      --    jshort * (JNICALL *GetShortArrayElements)
      --      (JNIEnv *env, jshortArray array, jboolean *isCopy);
         type F_GetIntArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetIntArrayElements );
      --    jint * (JNICALL *GetIntArrayElements)
      --      (JNIEnv *env, jintArray array, jboolean *isCopy);
         type F_GetLongArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetLongArrayElements );
      --    jlong * (JNICALL *GetLongArrayElements)
      --      (JNIEnv *env, jlongArray array, jboolean *isCopy);
         type F_GetFloatArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetFloatArrayElements );
      --    jfloat * (JNICALL *GetFloatArrayElements)
      --      (JNIEnv *env, jfloatArray array, jboolean *isCopy);
         type F_GetDoubleArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_GetDoubleArrayElements );
      --    jdouble * (JNICALL *GetDoubleArrayElements)
      --      (JNIEnv *env, jdoubleArray array, jboolean *isCopy);
      --
         type F_ReleaseBooleanArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseBooleanArrayElements );
      --    void (JNICALL *ReleaseBooleanArrayElements)
      --      (JNIEnv *env, jbooleanArray array, jboolean *elems, jint mode);
         type F_ReleaseByteArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseByteArrayElements );
      --    void (JNICALL *ReleaseByteArrayElements)
      --      (JNIEnv *env, jbyteArray array, jbyte *elems, jint mode);
         type F_ReleaseCharArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseCharArrayElements );
      --    void (JNICALL *ReleaseCharArrayElements)
      --      (JNIEnv *env, jcharArray array, jchar *elems, jint mode);
         type F_ReleaseShortArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseShortArrayElements );
      --    void (JNICALL *ReleaseShortArrayElements)
      --      (JNIEnv *env, jshortArray array, jshort *elems, jint mode);
         type F_ReleaseIntArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseIntArrayElements );
      --    void (JNICALL *ReleaseIntArrayElements)
      --      (JNIEnv *env, jintArray array, jint *elems, jint mode);
         type F_ReleaseLongArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseLongArrayElements );
      --    void (JNICALL *ReleaseLongArrayElements)
      --      (JNIEnv *env, jlongArray array, jlong *elems, jint mode);
         type F_ReleaseFloatArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseFloatArrayElements );
      --    void (JNICALL *ReleaseFloatArrayElements)
      --      (JNIEnv *env, jfloatArray array, jfloat *elems, jint mode);
         type F_ReleaseDoubleArrayElements  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseDoubleArrayElements );
      --    void (JNICALL *ReleaseDoubleArrayElements)
      --      (JNIEnv *env, jdoubleArray array, jdouble *elems, jint mode);
      --
         type F_GetBooleanArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetBooleanArrayRegion );
      --    void (JNICALL *GetBooleanArrayRegion)
      --      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, jboolean *buf);
         type F_GetByteArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetByteArrayRegion );
      --    void (JNICALL *GetByteArrayRegion)
      --      (JNIEnv *env, jbyteArray array, jsize start, jsize len, jbyte *buf);
         type F_GetCharArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetCharArrayRegion );
      --    void (JNICALL *GetCharArrayRegion)
      --      (JNIEnv *env, jcharArray array, jsize start, jsize len, jchar *buf);
         type F_GetShortArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetShortArrayRegion );
      --    void (JNICALL *GetShortArrayRegion)
      --      (JNIEnv *env, jshortArray array, jsize start, jsize len, jshort *buf);
         type F_GetIntArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetIntArrayRegion );
      --    void (JNICALL *GetIntArrayRegion)
      --      (JNIEnv *env, jintArray array, jsize start, jsize len, jint *buf);
         type F_GetLongArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetLongArrayRegion );
      --    void (JNICALL *GetLongArrayRegion)
      --      (JNIEnv *env, jlongArray array, jsize start, jsize len, jlong *buf);
         type F_GetFloatArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetFloatArrayRegion );
      --    void (JNICALL *GetFloatArrayRegion)
      --      (JNIEnv *env, jfloatArray array, jsize start, jsize len, jfloat *buf);
         type F_GetDoubleArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetDoubleArrayRegion );
      --    void (JNICALL *GetDoubleArrayRegion)
      --      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, jdouble *buf);
      --
         type F_SetBooleanArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetBooleanArrayRegion );
      --    void (JNICALL *SetBooleanArrayRegion)
      --      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, const jboolean *buf);
         type F_SetByteArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetByteArrayRegion );
      --    void (JNICALL *SetByteArrayRegion)
      --      (JNIEnv *env, jbyteArray array, jsize start, jsize len, const jbyte *buf);
         type F_SetCharArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetCharArrayRegion );
      --    void (JNICALL *SetCharArrayRegion)
      --      (JNIEnv *env, jcharArray array, jsize start, jsize len, const jchar *buf);
         type F_SetShortArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetShortArrayRegion );
      --    void (JNICALL *SetShortArrayRegion)
      --      (JNIEnv *env, jshortArray array, jsize start, jsize len, const jshort *buf);
         type F_SetIntArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetIntArrayRegion );
      --    void (JNICALL *SetIntArrayRegion)
      --      (JNIEnv *env, jintArray array, jsize start, jsize len, const jint *buf);
         type F_SetLongArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetLongArrayRegion );
      --    void (JNICALL *SetLongArrayRegion)
      --      (JNIEnv *env, jlongArray array, jsize start, jsize len, const jlong *buf);
         type F_SetFloatArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetFloatArrayRegion );
      --    void (JNICALL *SetFloatArrayRegion)
      --      (JNIEnv *env, jfloatArray array, jsize start, jsize len, const jfloat *buf);
         type F_SetDoubleArrayRegion  is new voidFunction;
         pragma Convention(StdCall, F_SetDoubleArrayRegion );
      --    void (JNICALL *SetDoubleArrayRegion)
      --      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, const jdouble *buf);
      --
         type F_RegisterNatives  is new voidFunction;
         pragma Convention(StdCall, F_RegisterNatives );
      --    jint (JNICALL *RegisterNatives)
      --      (JNIEnv *env, jclass clazz, const JNINativeMethod *methods,
      --        jint nMethods);
         type F_UnregisterNatives  is new voidFunction;
         pragma Convention(StdCall, F_UnregisterNatives );
      --    jint (JNICALL *UnregisterNatives)
      --      (JNIEnv *env, jclass clazz);
      --
         type F_MonitorEnter  is access function(E: JNIEnv; Obj: jobject) return jint;
         pragma Convention(StdCall, F_MonitorEnter );
      --    jint (JNICALL *MonitorEnter)
      --      (JNIEnv *env, jobject obj);
         type F_MonitorExit  is access function(E: JNIEnv; Obj: jobject) return jint;
         pragma Convention(StdCall, F_MonitorExit );
      --    jint (JNICALL *MonitorExit)
      --      (JNIEnv *env, jobject obj);
      --
         type F_GetJavaVM  is new voidFunction;
         pragma Convention(StdCall, F_GetJavaVM );
      --    jint (JNICALL *GetJavaVM)
      --      (JNIEnv *env, JavaVM **vm);
      --
         type F_GetStringRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetStringRegion );
      --    void (JNICALL *GetStringRegion)
      --      (JNIEnv *env, jstring str, jsize start, jsize len, jchar *buf);
         type F_GetStringUTFRegion  is new voidFunction;
         pragma Convention(StdCall, F_GetStringUTFRegion );
      --    void (JNICALL *GetStringUTFRegion)
      --      (JNIEnv *env, jstring str, jsize start, jsize len, char *buf);
      --
         type F_GetPrimitiveArrayCritical  is access function (E: JNIEnv; A: jarray; isCopy: jboolean_ptr) return System.Address;
         pragma Convention(StdCall, F_GetPrimitiveArrayCritical );
      --    void * (JNICALL *GetPrimitiveArrayCritical)
      --      (JNIEnv *env, jarray array, jboolean *isCopy);
         type F_ReleasePrimitiveArrayCritical  is access procedure (E: JNIEnv; A: jarray; CArray: System.Address; mode: jint);
         pragma Convention(StdCall, F_ReleasePrimitiveArrayCritical );
      --    void (JNICALL *ReleasePrimitiveArrayCritical)
      --      (JNIEnv *env, jarray array, void *carray, jint mode);
      --
         type F_GetStringCritical  is new voidFunction;
         pragma Convention(StdCall, F_GetStringCritical );
      --    const jchar * (JNICALL *GetStringCritical)
      --      (JNIEnv *env, jstring string, jboolean *isCopy);
         type F_ReleaseStringCritical  is new voidFunction;
         pragma Convention(StdCall, F_ReleaseStringCritical );
      --    void (JNICALL *ReleaseStringCritical)
      --      (JNIEnv *env, jstring string, const jchar *cstring);
      --
         type F_NewWeakGlobalRef  is new voidFunction;
         pragma Convention(StdCall, F_NewWeakGlobalRef );
      --    jweak (JNICALL *NewWeakGlobalRef)
      --       (JNIEnv *env, jobject obj);
         type F_DeleteWeakGlobalRef  is new voidFunction;
         pragma Convention(StdCall, F_DeleteWeakGlobalRef );
      --    void (JNICALL *DeleteWeakGlobalRef)
      --       (JNIEnv *env, jweak ref);
      --
         type F_ExceptionCheck  is access function (E: JNIEnv) return jboolean;
         pragma Convention(StdCall, F_ExceptionCheck );
      --    jboolean (JNICALL *ExceptionCheck)
      --       (JNIEnv *env);
      --
         type F_NewDirectByteBuffer  is new voidFunction;
         pragma Convention(StdCall, F_NewDirectByteBuffer );
      --    jobject (JNICALL *NewDirectByteBuffer)
      --       (JNIEnv* env, void* address, jlong capacity);
         type F_GetDirectBufferAddress  is new voidFunction;
         pragma Convention(StdCall, F_GetDirectBufferAddress );
      --    void* (JNICALL *GetDirectBufferAddress)
      --       (JNIEnv* env, jobject buf);
         type F_GetDirectBufferCapacity  is new voidFunction;
         pragma Convention(StdCall, F_GetDirectBufferCapacity );
      --    jlong (JNICALL *GetDirectBufferCapacity)
      --       (JNIEnv* env, jobject buf);
      -- };
	

   --------------------------------------------------
   -- Declaration of JNI interface
   type t_JNINativeInterface is
      record
      -- struct JNINativeInterface_ {
         reserved0  : F_reserved0 ;
      -- void *reserved0;
         reserved1  : F_reserved1 ;
      -- void *reserved1;
         reserved2  : F_reserved2 ;
      -- void *reserved2;
         reserved3  : F_reserved3 ;
         -- void *reserved3;

         GetVersion  : F_GetVersion ;
      -- jint (JNICALL *GetVersion)(JNIEnv *env);

         DefineClass  : F_DefineClass ;
    -- jclass (JNICALL *DefineClass)
      -- (JNIEnv *env, const char *name, jobject loader, const jbyte *buf,
    -- jsize len);
         FindClass  : F_FindClass ;
    -- jclass (JNICALL *FindClass)
      -- (JNIEnv *env, const char *name);

         FromReflectedMethod  : F_FromReflectedMethod ;
    -- jmethodID (JNICALL *FromReflectedMethod)
    -- (JNIEnv *env, jobject method);
         FromReflectedField  : F_FromReflectedField ;
    -- jfieldID (JNICALL *FromReflectedField)
      -- (JNIEnv *env, jobject field);

         ToReflectedMethod  : F_ToReflectedMethod ;
    -- jobject (JNICALL *ToReflectedMethod)
      -- (JNIEnv *env, jclass cls, jmethodID methodID, jboolean isStatic);

         GetSuperclass : F_GetSuperclass;
    -- jclass (JNICALL *GetSuperclass)
    -- (JNIEnv *env, jclass sub);
         IsAssignableFrom : F_IsAssignableFrom;
    -- jboolean (JNICALL *IsAssignableFrom)
      -- (JNIEnv *env, jclass sub, jclass sup);

         ToReflectedField : F_ToReflectedField;
    -- jobject (JNICALL *ToReflectedField)
      -- (JNIEnv *env, jclass cls, jfieldID fieldID, jboolean isStatic);

         Throw : F_Throw;
    -- jint (JNICALL *Throw)
    -- (JNIEnv *env, jthrowable obj);
         ThrowNew : F_ThrowNew;
    -- jint (JNICALL *ThrowNew)
    -- (JNIEnv *env, jclass clazz, const char *msg);
         ExceptionOccurred : F_ExceptionOccurred;
    -- jthrowable (JNICALL *ExceptionOccurred)
    -- (JNIEnv *env);
         ExceptionDescribe : F_ExceptionDescribe;
    -- void (JNICALL *ExceptionDescribe)
    -- (JNIEnv *env);
         ExceptionClear : F_ExceptionClear;
    -- void (JNICALL *ExceptionClear)
    -- (JNIEnv *env);
         FatalError : F_FatalError;
    -- void (JNICALL *FatalError)
      -- (JNIEnv *env, const char *msg);

         PushLocalFrame : F_PushLocalFrame;
    -- jint (JNICALL *PushLocalFrame)
    -- (JNIEnv *env, jint capacity);
         PopLocalFrame : F_PopLocalFrame;
    -- jobject (JNICALL *PopLocalFrame)
      -- (JNIEnv *env, jobject result);

         NewGlobalRef : F_NewGlobalRef;
    -- jobject (JNICALL *NewGlobalRef)
    -- (JNIEnv *env, jobject lobj);
         DeleteGlobalRef : F_DeleteGlobalRef;
    -- void (JNICALL *DeleteGlobalRef)
    -- (JNIEnv *env, jobject gref);
         DeleteLocalRef : F_DeleteLocalRef;
    -- void (JNICALL *DeleteLocalRef)
    -- (JNIEnv *env, jobject obj);
         IsSameObject : F_IsSameObject;
    -- jboolean (JNICALL *IsSameObject)
    -- (JNIEnv *env, jobject obj1, jobject obj2);
         NewLocalRef : F_NewLocalRef;
    -- jobject (JNICALL *NewLocalRef)
    -- (JNIEnv *env, jobject ref);
         EnsureLocalCapacity : F_EnsureLocalCapacity;
    -- jint (JNICALL *EnsureLocalCapacity)
      -- (JNIEnv *env, jint capacity);

         AllocObject : F_AllocObject;
    -- jobject (JNICALL *AllocObject)
    -- (JNIEnv *env, jclass clazz);
         NewObject : F_NewObject;
    -- jobject (JNICALL *NewObject)
    -- (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         NewObjectV : F_NewObjectV;
    -- jobject (JNICALL *NewObjectV)
    -- (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         NewObjectA : F_NewObjectA;
    -- jobject (JNICALL *NewObjectA)
      -- (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);

         GetObjectClass : F_GetObjectClass;
    -- jclass (JNICALL *GetObjectClass)
    -- (JNIEnv *env, jobject obj);
         IsInstanceOf : F_IsInstanceOf;
    -- jboolean (JNICALL *IsInstanceOf)
      -- (JNIEnv *env, jobject obj, jclass clazz);

         GetMethodID  : F_GetMethodID ;
    -- jmethodID (JNICALL *GetMethodID)
      -- (JNIEnv *env, jclass clazz, const char *name, const char *sig);

         CallObjectMethod : F_CallObjectMethod;
    -- jobject (JNICALL *CallObjectMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallObjectMethodV : F_CallObjectMethodV;
    -- jobject (JNICALL *CallObjectMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallObjectMethodA : F_CallObjectMethodA;
    -- jobject (JNICALL *CallObjectMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);

         CallBooleanMethod : F_CallBooleanMethod;
    -- jboolean (JNICALL *CallBooleanMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallBooleanMethodV : F_CallBooleanMethodV;
    -- jboolean (JNICALL *CallBooleanMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallBooleanMethodA : F_CallBooleanMethodA;
    -- jboolean (JNICALL *CallBooleanMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);

         CallByteMethod : F_CallByteMethod;
    -- jbyte (JNICALL *CallByteMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallByteMethodV : F_CallByteMethodV;
    -- jbyte (JNICALL *CallByteMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallByteMethodA : F_CallByteMethodA;
    -- jbyte (JNICALL *CallByteMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         CallCharMethod : F_CallCharMethod;
    -- jchar (JNICALL *CallCharMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallCharMethodV : F_CallCharMethodV;
    -- jchar (JNICALL *CallCharMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallCharMethodA : F_CallCharMethodA;
    -- jchar (JNICALL *CallCharMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         CallShortMethod : F_CallShortMethod;
    -- jshort (JNICALL *CallShortMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallShortMethodV : F_CallShortMethodV;
    -- jshort (JNICALL *CallShortMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallShortMethodA : F_CallShortMethodA;
    -- jshort (JNICALL *CallShortMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         CallIntMethod : F_CallIntMethod;
    -- jint (JNICALL *CallIntMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallIntMethodV : F_CallIntMethodV;
    -- jint (JNICALL *CallIntMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallIntMethodA : F_CallIntMethodA;
    -- jint (JNICALL *CallIntMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         CallLongMethod : F_CallLongMethod;
    -- jlong (JNICALL *CallLongMethod)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallLongMethodV : F_CallLongMethodV;
    -- jlong (JNICALL *CallLongMethodV)
    -- (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallLongMethodA : F_CallLongMethodA;
    -- jlong (JNICALL *CallLongMethodA)
      -- (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);

         CallFloatMethod  : F_CallFloatMethod ;
      --    jfloat (JNICALL *CallFloatMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallFloatMethodV  : F_CallFloatMethodV ;
      --    jfloat (JNICALL *CallFloatMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallFloatMethodA  : F_CallFloatMethodA ;
      --    jfloat (JNICALL *CallFloatMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);
      --
         CallDoubleMethod  : F_CallDoubleMethod ;
      --    jdouble (JNICALL *CallDoubleMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallDoubleMethodV  : F_CallDoubleMethodV ;
      --    jdouble (JNICALL *CallDoubleMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallDoubleMethodA  : F_CallDoubleMethodA ;
      --    jdouble (JNICALL *CallDoubleMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue *args);
      --
         CallVoidMethod  : F_CallVoidMethod ;
      --    void (JNICALL *CallVoidMethod)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, ...);
         CallVoidMethodV  : F_CallVoidMethodV ;
      --    void (JNICALL *CallVoidMethodV)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, va_list args);
         CallVoidMethodA  : F_CallVoidMethodA ;
      --    void (JNICALL *CallVoidMethodA)
      --      (JNIEnv *env, jobject obj, jmethodID methodID, const jvalue * args);
      --
         CallNonvirtualObjectMethod  : F_CallNonvirtualObjectMethod ;
      --    jobject (JNICALL *CallNonvirtualObjectMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualObjectMethodV  : F_CallNonvirtualObjectMethodV ;
      --    jobject (JNICALL *CallNonvirtualObjectMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualObjectMethodA : F_CallNonvirtualObjectMethodA;
      --    jobject (JNICALL *CallNonvirtualObjectMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
         CallNonvirtualBooleanMethod  : F_CallNonvirtualBooleanMethod ;
      --    jboolean (JNICALL *CallNonvirtualBooleanMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualBooleanMethodV  : F_CallNonvirtualBooleanMethodV ;
      --    jboolean (JNICALL *CallNonvirtualBooleanMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualBooleanMethodA : F_CallNonvirtualBooleanMethodA;
      --    jboolean (JNICALL *CallNonvirtualBooleanMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
         CallNonvirtualByteMethod  : F_CallNonvirtualByteMethod ;
      --    jbyte (JNICALL *CallNonvirtualByteMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualByteMethodV  : F_CallNonvirtualByteMethodV ;
      --    jbyte (JNICALL *CallNonvirtualByteMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualByteMethodA : F_CallNonvirtualByteMethodA;
      --    jbyte (JNICALL *CallNonvirtualByteMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualCharMethod  : F_CallNonvirtualCharMethod ;
      --    jchar (JNICALL *CallNonvirtualCharMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualCharMethodV  : F_CallNonvirtualCharMethodV ;
      --    jchar (JNICALL *CallNonvirtualCharMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualCharMethodA : F_CallNonvirtualCharMethodA;
      --    jchar (JNICALL *CallNonvirtualCharMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualShortMethod  : F_CallNonvirtualShortMethod ;
      --    jshort (JNICALL *CallNonvirtualShortMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualShortMethodV  : F_CallNonvirtualShortMethodV ;
      --    jshort (JNICALL *CallNonvirtualShortMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualShortMethodA : F_CallNonvirtualShortMethodA;
      --    jshort (JNICALL *CallNonvirtualShortMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualIntMethod  : F_CallNonvirtualIntMethod ;
      --    jint (JNICALL *CallNonvirtualIntMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualIntMethodV  : F_CallNonvirtualIntMethodV ;
      --    jint (JNICALL *CallNonvirtualIntMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualIntMethodA : F_CallNonvirtualIntMethodA;
      --    jint (JNICALL *CallNonvirtualIntMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualLongMethod  : F_CallNonvirtualLongMethod ;
      --    jlong (JNICALL *CallNonvirtualLongMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualLongMethodV  : F_CallNonvirtualLongMethodV ;
      --    jlong (JNICALL *CallNonvirtualLongMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualLongMethodA : F_CallNonvirtualLongMethodA;
      --    jlong (JNICALL *CallNonvirtualLongMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualFloatMethod  : F_CallNonvirtualFloatMethod ;
      --    jfloat (JNICALL *CallNonvirtualFloatMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualFloatMethodV  : F_CallNonvirtualFloatMethodV ;
      --    jfloat (JNICALL *CallNonvirtualFloatMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualFloatMethodA : F_CallNonvirtualFloatMethodA;
      --    jfloat (JNICALL *CallNonvirtualFloatMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualDoubleMethod  : F_CallNonvirtualDoubleMethod ;
      --    jdouble (JNICALL *CallNonvirtualDoubleMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualDoubleMethodV  : F_CallNonvirtualDoubleMethodV ;
      --    jdouble (JNICALL *CallNonvirtualDoubleMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualDoubleMethodA : F_CallNonvirtualDoubleMethodA;
      --    jdouble (JNICALL *CallNonvirtualDoubleMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue *args);
      --
         CallNonvirtualVoidMethod  : F_CallNonvirtualVoidMethod ;
      --    void (JNICALL *CallNonvirtualVoidMethod)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID, ...);
         CallNonvirtualVoidMethodV  : F_CallNonvirtualVoidMethodV ;
      --    void (JNICALL *CallNonvirtualVoidMethodV)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        va_list args);
         CallNonvirtualVoidMethodA : F_CallNonvirtualVoidMethodA;
      --    void (JNICALL *CallNonvirtualVoidMethodA)
      --      (JNIEnv *env, jobject obj, jclass clazz, jmethodID methodID,
      --        const jvalue * args);
      --
         GetFieldID: F_GetFieldID;
      --    jfieldID (JNICALL *GetFieldID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
      --
         GetObjectField : F_GetObjectField;
      --    jobject (JNICALL *GetObjectField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetBooleanField : F_GetBooleanField;
      --    jboolean (JNICALL *GetBooleanField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetByteField : F_GetByteField;
      --    jbyte (JNICALL *GetByteField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetCharField : F_GetCharField;
      --    jchar (JNICALL *GetCharField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetShortField : F_GetShortField;
      --    jshort (JNICALL *GetShortField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetIntField : F_GetIntField;
      --    jint (JNICALL *GetIntField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetLongField : F_GetLongField;
      --    jlong (JNICALL *GetLongField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetFloatField : F_GetFloatField;
      --    jfloat (JNICALL *GetFloatField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
         GetDoubleField : F_GetDoubleField;
      --    jdouble (JNICALL *GetDoubleField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID);
      --
         SetObjectField : F_SetObjectField;
      --    void (JNICALL *SetObjectField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jobject val);
         SetBooleanField : F_SetBooleanField;
      --    void (JNICALL *SetBooleanField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jboolean val);
         SetByteField : F_SetByteField;
      --    void (JNICALL *SetByteField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jbyte val);
         SetCharField : F_SetCharField;
      --    void (JNICALL *SetCharField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jchar val);
         SetShortField : F_SetShortField;
      --    void (JNICALL *SetShortField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jshort val);
         SetIntField : F_SetIntField;
      --    void (JNICALL *SetIntField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jint val);
         SetLongField : F_SetLongField;
      --    void (JNICALL *SetLongField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jlong val);
         SetFloatField : F_SetFloatField;
      --    void (JNICALL *SetFloatField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jfloat val);
         SetDoubleField : F_SetDoubleField;
      --    void (JNICALL *SetDoubleField)
      --      (JNIEnv *env, jobject obj, jfieldID fieldID, jdouble val);
      --
         GetStaticMethodID : F_GetStaticMethodID;
      --    jmethodID (JNICALL *GetStaticMethodID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
      --
         CallStaticObjectMethod  : F_CallStaticObjectMethod ;
      --    jobject (JNICALL *CallStaticObjectMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticObjectMethodV  : F_CallStaticObjectMethodV ;
      --    jobject (JNICALL *CallStaticObjectMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticObjectMethodA : F_CallStaticObjectMethodA;
      --    jobject (JNICALL *CallStaticObjectMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticBooleanMethod  : F_CallStaticBooleanMethod ;
      --    jboolean (JNICALL *CallStaticBooleanMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticBooleanMethodV  : F_CallStaticBooleanMethodV ;
      --    jboolean (JNICALL *CallStaticBooleanMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticBooleanMethodA : F_CallStaticBooleanMethodA;
      --    jboolean (JNICALL *CallStaticBooleanMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticByteMethod  : F_CallStaticByteMethod ;
      --    jbyte (JNICALL *CallStaticByteMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticByteMethodV  : F_CallStaticByteMethodV ;
      --    jbyte (JNICALL *CallStaticByteMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticByteMethodA : F_CallStaticByteMethodA;
      --    jbyte (JNICALL *CallStaticByteMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticCharMethod  : F_CallStaticCharMethod ;
      --    jchar (JNICALL *CallStaticCharMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticCharMethodV  : F_CallStaticCharMethodV ;
      --    jchar (JNICALL *CallStaticCharMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticCharMethodA : F_CallStaticCharMethodA;
      --    jchar (JNICALL *CallStaticCharMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticShortMethod  : F_CallStaticShortMethod ;
      --    jshort (JNICALL *CallStaticShortMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticShortMethodV  : F_CallStaticShortMethodV ;
      --    jshort (JNICALL *CallStaticShortMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticShortMethodA : F_CallStaticShortMethodA;
      --    jshort (JNICALL *CallStaticShortMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticIntMethod  : F_CallStaticIntMethod ;
      --    jint (JNICALL *CallStaticIntMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticIntMethodV  : F_CallStaticIntMethodV ;
      --    jint (JNICALL *CallStaticIntMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticIntMethodA : F_CallStaticIntMethodA;
      --    jint (JNICALL *CallStaticIntMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticLongMethod  : F_CallStaticLongMethod ;
      --    jlong (JNICALL *CallStaticLongMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticLongMethodV  : F_CallStaticLongMethodV ;
      --    jlong (JNICALL *CallStaticLongMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticLongMethodA : F_CallStaticLongMethodA;
      --    jlong (JNICALL *CallStaticLongMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticFloatMethod  : F_CallStaticFloatMethod ;
      --    jfloat (JNICALL *CallStaticFloatMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticFloatMethodV  : F_CallStaticFloatMethodV ;
      --    jfloat (JNICALL *CallStaticFloatMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticFloatMethodA : F_CallStaticFloatMethodA;
      --    jfloat (JNICALL *CallStaticFloatMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticDoubleMethod  : F_CallStaticDoubleMethod ;
      --    jdouble (JNICALL *CallStaticDoubleMethod)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, ...);
         CallStaticDoubleMethodV  : F_CallStaticDoubleMethodV ;
      --    jdouble (JNICALL *CallStaticDoubleMethodV)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, va_list args);
         CallStaticDoubleMethodA : F_CallStaticDoubleMethodA;
      --    jdouble (JNICALL *CallStaticDoubleMethodA)
      --      (JNIEnv *env, jclass clazz, jmethodID methodID, const jvalue *args);
      --
         CallStaticVoidMethod  : F_CallStaticVoidMethod ;
      --    void (JNICALL *CallStaticVoidMethod)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, ...);
         CallStaticVoidMethodV  : F_CallStaticVoidMethodV ;
      --    void (JNICALL *CallStaticVoidMethodV)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, va_list args);
         CallStaticVoidMethodA : F_CallStaticVoidMethodA;
      --    void (JNICALL *CallStaticVoidMethodA)
      --      (JNIEnv *env, jclass cls, jmethodID methodID, const jvalue * args);
      --

         GetStaticFieldID : F_GetStaticFieldID;
      --    jfieldID (JNICALL *GetStaticFieldID)
      --      (JNIEnv *env, jclass clazz, const char *name, const char *sig);
         GetStaticObjectField : F_GetStaticObjectField;
      --    jobject (JNICALL *GetStaticObjectField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticBooleanField : F_GetStaticBooleanField;
      --    jboolean (JNICALL *GetStaticBooleanField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticByteField : F_GetStaticByteField;
      --    jbyte (JNICALL *GetStaticByteField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticCharField : F_GetStaticCharField;
      --    jchar (JNICALL *GetStaticCharField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticShortField : F_GetStaticShortField;
      --    jshort (JNICALL *GetStaticShortField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticIntField : F_GetStaticIntField;
      --    jint (JNICALL *GetStaticIntField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticLongField : F_GetStaticLongField;
      --    jlong (JNICALL *GetStaticLongField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticFloatField : F_GetStaticFloatField;
      --    jfloat (JNICALL *GetStaticFloatField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
         GetStaticDoubleField : F_GetStaticDoubleField;
      --    jdouble (JNICALL *GetStaticDoubleField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID);
      --
         SetStaticObjectField : F_SetStaticObjectField;
      --    void (JNICALL *SetStaticObjectField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jobject value);
         SetStaticBooleanField : F_SetStaticBooleanField;
      --    void (JNICALL *SetStaticBooleanField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jboolean value);
         SetStaticByteField : F_SetStaticByteField;
      --    void (JNICALL *SetStaticByteField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jbyte value);
         SetStaticCharField : F_SetStaticCharField;
      --    void (JNICALL *SetStaticCharField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jchar value);
         SetStaticShortField : F_SetStaticShortField;
      --    void (JNICALL *SetStaticShortField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jshort value);
         SetStaticIntField : F_SetStaticIntField;
      --    void (JNICALL *SetStaticIntField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jint value);
         SetStaticLongField : F_SetStaticLongField;
      --    void (JNICALL *SetStaticLongField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jlong value);
         SetStaticFloatField : F_SetStaticFloatField;
      --    void (JNICALL *SetStaticFloatField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jfloat value);
         SetStaticDoubleField : F_SetStaticDoubleField;
      --    void (JNICALL *SetStaticDoubleField)
      --      (JNIEnv *env, jclass clazz, jfieldID fieldID, jdouble value);

         NewString  : F_NewString ;
      --    jstring (JNICALL *NewString)
      --      (JNIEnv *env, const jchar *unicode, jsize len);
         GetStringLength  : F_GetStringLength ;
      --    jsize (JNICALL *GetStringLength)
      --      (JNIEnv *env, jstring str);
         GetStringChars  : F_GetStringChars ;
      --    const jchar *(JNICALL *GetStringChars)
      --      (JNIEnv *env, jstring str, jboolean *isCopy);
         ReleaseStringChars  : F_ReleaseStringChars ;
      --    void (JNICALL *ReleaseStringChars)
      --      (JNIEnv *env, jstring str, const jchar *chars);
      --
         NewStringUTF  : F_NewStringUTF ;
      --    jstring (JNICALL *NewStringUTF)
      --      (JNIEnv *env, const char *utf);
         GetStringUTFLength  : F_GetStringUTFLength ;
      --    jsize (JNICALL *GetStringUTFLength)
      --      (JNIEnv *env, jstring str);
         GetStringUTFChars  : F_GetStringUTFChars ;
      --    const char* (JNICALL *GetStringUTFChars)
      --      (JNIEnv *env, jstring str, jboolean *isCopy);
         ReleaseStringUTFChars  : F_ReleaseStringUTFChars ;
      --    void (JNICALL *ReleaseStringUTFChars)
      --      (JNIEnv *env, jstring str, const char* chars);
      --
      --
         GetArrayLength  : F_GetArrayLength ;
      --    jsize (JNICALL *GetArrayLength)
      --      (JNIEnv *env, jarray array);
      --
         NewObjectArray  : F_NewObjectArray ;
      --    jobjectArray (JNICALL *NewObjectArray)
      --      (JNIEnv *env, jsize len, jclass clazz, jobject init);
         GetObjectArrayElement  : F_GetObjectArrayElement ;
      --    jobject (JNICALL *GetObjectArrayElement)
      --      (JNIEnv *env, jobjectArray array, jsize index);
         SetObjectArrayElement  : F_SetObjectArrayElement ;
      --    void (JNICALL *SetObjectArrayElement)
      --      (JNIEnv *env, jobjectArray array, jsize index, jobject val);
      --
         NewBooleanArray  : F_NewBooleanArray ;
      --    jbooleanArray (JNICALL *NewBooleanArray)
      --      (JNIEnv *env, jsize len);
         NewByteArray  : F_NewByteArray ;
      --    jbyteArray (JNICALL *NewByteArray)
      --      (JNIEnv *env, jsize len);
         NewCharArray  : F_NewCharArray ;
      --    jcharArray (JNICALL *NewCharArray)
      --      (JNIEnv *env, jsize len);
         NewShortArray  : F_NewShortArray ;
      --    jshortArray (JNICALL *NewShortArray)
      --      (JNIEnv *env, jsize len);
         NewIntArray  : F_NewIntArray ;
      --    jintArray (JNICALL *NewIntArray)
      --      (JNIEnv *env, jsize len);
         NewLongArray  : F_NewLongArray ;
      --    jlongArray (JNICALL *NewLongArray)
      --      (JNIEnv *env, jsize len);
         NewFloatArray  : F_NewFloatArray ;
      --    jfloatArray (JNICALL *NewFloatArray)
      --      (JNIEnv *env, jsize len);
         NewDoubleArray  : F_NewDoubleArray ;
      --    jdoubleArray (JNICALL *NewDoubleArray)
      --      (JNIEnv *env, jsize len);
      --
         GetBooleanArrayElements  : F_GetBooleanArrayElements ;
      --    jboolean * (JNICALL *GetBooleanArrayElements)
      --      (JNIEnv *env, jbooleanArray array, jboolean *isCopy);
         GetByteArrayElements  : F_GetByteArrayElements ;
      --    jbyte * (JNICALL *GetByteArrayElements)
      --      (JNIEnv *env, jbyteArray array, jboolean *isCopy);
         GetCharArrayElements  : F_GetCharArrayElements ;
      --    jchar * (JNICALL *GetCharArrayElements)
      --      (JNIEnv *env, jcharArray array, jboolean *isCopy);
         GetShortArrayElements  : F_GetShortArrayElements ;
      --    jshort * (JNICALL *GetShortArrayElements)
      --      (JNIEnv *env, jshortArray array, jboolean *isCopy);
         GetIntArrayElements  : F_GetIntArrayElements ;
      --    jint * (JNICALL *GetIntArrayElements)
      --      (JNIEnv *env, jintArray array, jboolean *isCopy);
         GetLongArrayElements  : F_GetLongArrayElements ;
      --    jlong * (JNICALL *GetLongArrayElements)
      --      (JNIEnv *env, jlongArray array, jboolean *isCopy);
         GetFloatArrayElements  : F_GetFloatArrayElements ;
      --    jfloat * (JNICALL *GetFloatArrayElements)
      --      (JNIEnv *env, jfloatArray array, jboolean *isCopy);
         GetDoubleArrayElements  : F_GetDoubleArrayElements ;
      --    jdouble * (JNICALL *GetDoubleArrayElements)
      --      (JNIEnv *env, jdoubleArray array, jboolean *isCopy);
      --
         ReleaseBooleanArrayElements  : F_ReleaseBooleanArrayElements ;
      --    void (JNICALL *ReleaseBooleanArrayElements)
      --      (JNIEnv *env, jbooleanArray array, jboolean *elems, jint mode);
         ReleaseByteArrayElements  : F_ReleaseByteArrayElements ;
      --    void (JNICALL *ReleaseByteArrayElements)
      --      (JNIEnv *env, jbyteArray array, jbyte *elems, jint mode);
         ReleaseCharArrayElements  : F_ReleaseCharArrayElements ;
      --    void (JNICALL *ReleaseCharArrayElements)
      --      (JNIEnv *env, jcharArray array, jchar *elems, jint mode);
         ReleaseShortArrayElements  : F_ReleaseShortArrayElements ;
      --    void (JNICALL *ReleaseShortArrayElements)
      --      (JNIEnv *env, jshortArray array, jshort *elems, jint mode);
         ReleaseIntArrayElements  : F_ReleaseIntArrayElements ;
      --    void (JNICALL *ReleaseIntArrayElements)
      --      (JNIEnv *env, jintArray array, jint *elems, jint mode);
         ReleaseLongArrayElements  : F_ReleaseLongArrayElements ;
      --    void (JNICALL *ReleaseLongArrayElements)
      --      (JNIEnv *env, jlongArray array, jlong *elems, jint mode);
         ReleaseFloatArrayElements  : F_ReleaseFloatArrayElements ;
      --    void (JNICALL *ReleaseFloatArrayElements)
      --      (JNIEnv *env, jfloatArray array, jfloat *elems, jint mode);
         ReleaseDoubleArrayElements  : F_ReleaseDoubleArrayElements ;
      --    void (JNICALL *ReleaseDoubleArrayElements)
      --      (JNIEnv *env, jdoubleArray array, jdouble *elems, jint mode);
      --
         GetBooleanArrayRegion  : F_GetBooleanArrayRegion ;
      --    void (JNICALL *GetBooleanArrayRegion)
      --      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, jboolean *buf);
         GetByteArrayRegion  : F_GetByteArrayRegion ;
      --    void (JNICALL *GetByteArrayRegion)
      --      (JNIEnv *env, jbyteArray array, jsize start, jsize len, jbyte *buf);
         GetCharArrayRegion  : F_GetCharArrayRegion ;
      --    void (JNICALL *GetCharArrayRegion)
      --      (JNIEnv *env, jcharArray array, jsize start, jsize len, jchar *buf);
         GetShortArrayRegion  : F_GetShortArrayRegion ;
      --    void (JNICALL *GetShortArrayRegion)
      --      (JNIEnv *env, jshortArray array, jsize start, jsize len, jshort *buf);
         GetIntArrayRegion  : F_GetIntArrayRegion ;
      --    void (JNICALL *GetIntArrayRegion)
      --      (JNIEnv *env, jintArray array, jsize start, jsize len, jint *buf);
         GetLongArrayRegion  : F_GetLongArrayRegion ;
      --    void (JNICALL *GetLongArrayRegion)
      --      (JNIEnv *env, jlongArray array, jsize start, jsize len, jlong *buf);
         GetFloatArrayRegion  : F_GetFloatArrayRegion ;
      --    void (JNICALL *GetFloatArrayRegion)
      --      (JNIEnv *env, jfloatArray array, jsize start, jsize len, jfloat *buf);
         GetDoubleArrayRegion  : F_GetDoubleArrayRegion ;
      --    void (JNICALL *GetDoubleArrayRegion)
      --      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, jdouble *buf);
      --
         SetBooleanArrayRegion  : F_SetBooleanArrayRegion ;
      --    void (JNICALL *SetBooleanArrayRegion)
      --      (JNIEnv *env, jbooleanArray array, jsize start, jsize l, const jboolean *buf);
         SetByteArrayRegion  : F_SetByteArrayRegion ;
      --    void (JNICALL *SetByteArrayRegion)
      --      (JNIEnv *env, jbyteArray array, jsize start, jsize len, const jbyte *buf);
         SetCharArrayRegion  : F_SetCharArrayRegion ;
      --    void (JNICALL *SetCharArrayRegion)
      --      (JNIEnv *env, jcharArray array, jsize start, jsize len, const jchar *buf);
         SetShortArrayRegion  : F_SetShortArrayRegion ;
      --    void (JNICALL *SetShortArrayRegion)
      --      (JNIEnv *env, jshortArray array, jsize start, jsize len, const jshort *buf);
         SetIntArrayRegion  : F_SetIntArrayRegion ;
      --    void (JNICALL *SetIntArrayRegion)
      --      (JNIEnv *env, jintArray array, jsize start, jsize len, const jint *buf);
         SetLongArrayRegion  : F_SetLongArrayRegion ;
      --    void (JNICALL *SetLongArrayRegion)
      --      (JNIEnv *env, jlongArray array, jsize start, jsize len, const jlong *buf);
         SetFloatArrayRegion  : F_SetFloatArrayRegion ;
      --    void (JNICALL *SetFloatArrayRegion)
      --      (JNIEnv *env, jfloatArray array, jsize start, jsize len, const jfloat *buf);
         SetDoubleArrayRegion  : F_SetDoubleArrayRegion ;
      --    void (JNICALL *SetDoubleArrayRegion)
      --      (JNIEnv *env, jdoubleArray array, jsize start, jsize len, const jdouble *buf);
      --
         RegisterNatives  : F_RegisterNatives ;
      --    jint (JNICALL *RegisterNatives)
      --      (JNIEnv *env, jclass clazz, const JNINativeMethod *methods,
      --        jint nMethods);
         UnregisterNatives  : F_UnregisterNatives ;
      --    jint (JNICALL *UnregisterNatives)
      --      (JNIEnv *env, jclass clazz);
      --
         MonitorEnter  : F_MonitorEnter ;
      --    jint (JNICALL *MonitorEnter)
      --      (JNIEnv *env, jobject obj);
         MonitorExit  : F_MonitorExit ;
      --    jint (JNICALL *MonitorExit)
      --      (JNIEnv *env, jobject obj);
      --
         GetJavaVM  : F_GetJavaVM ;
      --    jint (JNICALL *GetJavaVM)
      --      (JNIEnv *env, JavaVM **vm);
      --
         GetStringRegion  : F_GetStringRegion ;
      --    void (JNICALL *GetStringRegion)
      --      (JNIEnv *env, jstring str, jsize start, jsize len, jchar *buf);
         GetStringUTFRegion  : F_GetStringUTFRegion ;
      --    void (JNICALL *GetStringUTFRegion)
      --      (JNIEnv *env, jstring str, jsize start, jsize len, char *buf);
      --
         GetPrimitiveArrayCritical  : F_GetPrimitiveArrayCritical ;
      --    void * (JNICALL *GetPrimitiveArrayCritical)
      --      (JNIEnv *env, jarray array, jboolean *isCopy);
         ReleasePrimitiveArrayCritical  : F_ReleasePrimitiveArrayCritical ;
      --    void (JNICALL *ReleasePrimitiveArrayCritical)
      --      (JNIEnv *env, jarray array, void *carray, jint mode);
      --
         GetStringCritical  : F_GetStringCritical ;
      --    const jchar * (JNICALL *GetStringCritical)
      --      (JNIEnv *env, jstring string, jboolean *isCopy);
         ReleaseStringCritical  : F_ReleaseStringCritical ;
      --    void (JNICALL *ReleaseStringCritical)
      --      (JNIEnv *env, jstring string, const jchar *cstring);
      --
         NewWeakGlobalRef  : F_NewWeakGlobalRef ;
      --    jweak (JNICALL *NewWeakGlobalRef)
      --       (JNIEnv *env, jobject obj);
         DeleteWeakGlobalRef  : F_DeleteWeakGlobalRef ;
      --    void (JNICALL *DeleteWeakGlobalRef)
      --       (JNIEnv *env, jweak ref);
      --
         ExceptionCheck  : F_ExceptionCheck ;
      --    jboolean (JNICALL *ExceptionCheck)
      --       (JNIEnv *env);
      --
         NewDirectByteBuffer  : F_NewDirectByteBuffer ;
      --    jobject (JNICALL *NewDirectByteBuffer)
      --       (JNIEnv* env, void* address, jlong capacity);
         GetDirectBufferAddress  : F_GetDirectBufferAddress ;
      --    void* (JNICALL *GetDirectBufferAddress)
      --       (JNIEnv* env, jobject buf);
         GetDirectBufferCapacity  : F_GetDirectBufferCapacity ;
      --    jlong (JNICALL *GetDirectBufferCapacity)
      --       (JNIEnv* env, jobject buf);
      -- };
      end record; --t_JNINativeInterface
   pragma Convention(C, t_JNINativeInterface);

-- <Removed C++ interface>

   type JavaVMOption is
      record
         optionString: access C.Char;
         extraInfo: access C.Char; -- TODO: is this appropriate?
      end record;
-- typedef struct JavaVMOption {
    -- char *optionString;
    -- void *extraInfo;
-- } JavaVMOption;

   type options_array is array (C.size_t range <>) of aliased JavaVMOption;
   type JavaVMInitArgs is
      record
         version: jint;
         nOptions: jint;
         options: access options_array;
         ignoreUnrecognized: jboolean;
      end record;
-- typedef struct JavaVMInitArgs {
    -- jint version;

    -- jint nOptions;
    -- JavaVMOption *options;
    -- jboolean ignoreUnrecognized;
-- } JavaVMInitArgs;

   type JavaVMAttachArgs is
      record
         version: jint;
         name: access C.Char;
         group: jobject;
      end record;
   -- typedef struct JavaVMAttachArgs {
    -- jint version;

    -- char *name;
    -- jobject group;
-- } JavaVMAttachArgs;

-- /* These structures will be VM-specific. */

-- typedef struct JDK1_1InitArgs {
    -- jint version;

    -- char **properties;
    -- jint checkSource;
    -- jint nativeStackSize;
    -- jint javaStackSize;
    -- jint minHeapSize;
    -- jint maxHeapSize;
    -- jint verifyMode;
    -- char *classpath;

    -- jint (JNICALL *vfprintf)(FILE *fp, const char *format, va_list args);
    -- void (JNICALL *exit)(jint code);
    -- void (JNICALL *abort)(void);

    -- jint enableClassGC;
    -- jint enableVerboseGC;
    -- jint disableAsyncGC;
    -- jint verbose;
    -- jboolean debugging;
    -- jint debugPort;
-- } JDK1_1InitArgs;

-- typedef struct JDK1_1AttachArgs {
    -- void * __padding; /* C compilers don't allow empty structures. */
-- } JDK1_1AttachArgs;

-- #define JDK1_2
-- #define JDK1_4

-- /* End VM-specific. */

   type t_JNIInvokeInterface is
      record
         -- struct JNIInvokeInterface_ {
         reserved0 : voidFunction;
         -- void *reserved0;
         reserved1 : voidFunction;
         -- void *reserved1;
         reserved2 : voidFunction;
    -- void *reserved2;

         DestroyJavaVM: voidFunction;
    -- jint (JNICALL *DestroyJavaVM)(JavaVM *vm);
         AttachCurrentThread: voidFunction;
    -- jint (JNICALL *AttachCurrentThread)(JavaVM *vm, void **penv, void *args);
         DetachCurrentThread: voidFunction;
    -- jint (JNICALL *DetachCurrentThread)(JavaVM *vm);
         GetEnv: voidFunction;
    -- jint (JNICALL *GetEnv)(JavaVM *vm, void **penv, jint version);
         AttachCurrentThreadAsDaemon : voidFunction;
    -- jint (JNICALL *AttachCurrentThreadAsDaemon)(JavaVM *vm, void **penv, void *args);
    -- };
      end record;

-- <removed C++ stuff>

----
-- _JNI_IMPORT_OR_EXPORT_ jint JNICALL
-- JNI_GetDefaultJavaVMInitArgs(void *args);

-- _JNI_IMPORT_OR_EXPORT_ jint JNICALL
-- JNI_CreateJavaVM(JavaVM **pvm, void **penv, void *args);

-- _JNI_IMPORT_OR_EXPORT_ jint JNICALL
-- JNI_GetCreatedJavaVMs(JavaVM **, jsize, jsize *);

-- /* Defined by native libraries. */
-- JNIEXPORT jint JNICALL
-- JNI_OnLoad(JavaVM *vm, void *reserved);

-- JNIEXPORT void JNICALL
-- JNI_OnUnload(JavaVM *vm, void *reserved);

   ----
   -- IDs
   Class_Not_Found: exception;
   Method_Not_Found: exception;
   Field_Not_Found: exception;
   function findClass(E: JNIEnv; name: String) return jclass;
   function GetMethodID(E: JNIEnv; Clazz : Jclass; Name, Sig: String) return JmethodID;
   function GetStaticMethodID(E: JNIEnv; Clazz : Jclass; Name, Sig: String) return JmethodID;
   function getFieldID(E: JNIEnv; clazz : jclass; name, sig: String) return jfieldID;

   -- Strings
   function String_To_Ada(E: JNIEnv; S: Jstring) return String;
   function String_To_Java(E: JNIEnv; S: String) return Jstring;

   -- Bools
   function Boolean_To_Ada(E: JNIEnv; B: Jboolean) return Standard.Boolean;
   function Boolean_To_Java(E: JNIEnv; B: Standard.Boolean) return JBoolean;

   -- Arrays
   type Boolean_Array is array (Integer range <>) of Standard.Boolean;
   type Boolean_Array_Ptr is access Boolean_Array;
   function To_Ada(E: JNIEnv; A: jbooleanArray) return Boolean_Array_Ptr;
   function To_Java(E: JNIEnv; A: Boolean_Array_Ptr) return jbooleanArray;

   type Integer_Array is array (Integer range <>) of Integer;
   type Integer_Array_Ptr is access Integer_Array;
   function To_Ada(E: JNIEnv; A: jintArray) return Integer_Array_Ptr;
   function To_Java(E: JNIEnv; A: Integer_Array_Ptr) return jintArray;

--     function To_Ada(E: JNIEnv; A: jfloatArray) return array (Integer range <>) of Float;
--     function To_Ada(E: JNIEnv; A: jfloatArray) return array (Integer range <>) of Float;
--     function To_Ada(E: JNIEnv; A: jdoubleArray) return array (Integer range <>) of Double;
--     function To_Ada(E: JNIEnv; A: jdoubleArray) return array (Integer range <>) of Double;

   -- Proxy types
   type Proxy is interface;

   procedure Register(T: Tag; Class: String);
   function Get_Tag(Class: String) return Tag;
   type Params is
      record
         E: JNIEnv;
         Obj: JObject;
      end record;
   function New_Proxy(P: not null access Params) return Proxy is abstract;

   function Make_Proxy is new Ada.Tags.Generic_Dispatching_Constructor
     (Proxy, Params, New_Proxy);
   function Make_Proxy(E: JNIEnv; Obj: JObject) return Proxy'Class;

   Error: exception;

   -- Comparable objects
   type Comparable is interface;

   function Equals(Obj: Comparable; Obj2: Comparable'Class) return Standard.Boolean is abstract;
   function HashCode(Obj: Comparable) return Integer is abstract;

   --Exceptions
   procedure Raise_Java_Exception(E: JNIEnv; Message: String);
   procedure Raise_Java_Exception(E: JNIEnv; Event: Exception_Occurrence);

----
end Jni;
