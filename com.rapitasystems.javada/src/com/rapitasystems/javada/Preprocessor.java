/**
 * Javada - Copyright 2009  Rapita Systems Ltd.
 * http://www.rapitasystems.com  javada@rapitasystems.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rapitasystems.javada;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.eclipsecorba.idl.PreprocessorException;
import net.sf.eclipsecorba.idl.Specification;
import net.sf.eclipsecorba.idl.node.TDirective;
import net.sf.eclipsecorba.idl.parser.ParserException;

public class Preprocessor extends net.sf.eclipsecorba.idl.Preprocessor {
	List<Specification> includes = new ArrayList<Specification>();
	HashMap<String, String> definitions = new HashMap<String, String>();
	private final List<File> includeDirs;
	private final IdlParser parser;
	private final File parent;
	
	public Preprocessor(List<File> includeDirs, IdlParser parser, File parent) {
		this.includeDirs = includeDirs;
		this.parser = parser;
		this.parent = parent;
	}

	public void process(TDirective t) throws ParserException {
		if (t.getText().startsWith("#include ")) {
			Pattern pattern = Pattern.compile("[\"<](.+)[\">]");
			Matcher matcher = pattern.matcher(t.getText());
			Specification include = null;
			if (matcher.find()) {
				String filename = matcher.group(1);
				include = parser.parse(parent, filename, includeDirs);
				includes.add(include);
			}
			if (include == null) {
				throw new ParserException(t, "Unable to find include file for: " + t.getText());
			}
		}
		else if (t.getText().startsWith("#define ")) {
			String[] split = t.getText().trim().split("\\s+", 3);
			if (split.length < 2 || split.length > 3)
				throw new ParserException(t, "Unable to parse definition: " + t.getText());
			else {
                if (split.length == 2)
                    definitions.put(split[1], "TRUE");
                else
                    definitions.put(split[1], split[2]);
            }
		}
	}

	public List<Specification> getIncludes() {
		return includes;
	}

	@Override
	public HashMap<String, String> getDefinitions() {
		return definitions;
	}

	@Override
	protected void handle(PreprocessorException e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void include(String command, TDirective token)
			throws PreprocessorException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void pragma(String command, TDirective token)
			throws PreprocessorException {
		// TODO Auto-generated method stub
		
	}

}
