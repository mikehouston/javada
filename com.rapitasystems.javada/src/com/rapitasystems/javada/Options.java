/**
 * Javada - Copyright 2009  Rapita Systems Ltd.
 * http://www.rapitasystems.com  javada@rapitasystems.com
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.rapitasystems.javada;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Options {

	public class ArgumentException extends Exception {
		public ArgumentException(String message) {
			super(message);
		}
	}

	private List<File> includeDirs = new ArrayList<File>();
	private File inputFile;
	private File outputDirectory;
	private String javaPrefix;
	private String libraryName;
	private String arraySuffix;
	private String typePrefix;
	private String implSuffix;
	private String pointerSuffix;
	
	public Options() {
		
	}
	
	public Options(String[] args) throws ArgumentException {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			try {
				if (arg.equals("-I")) {
					File file = new File(args[++i]);
					if (!file.exists())
						throw new ArgumentException("Input directory '" + file + "' does not exist");
					getIncludeDirs().add(file);
				}
				else if (arg.equals("-O")) {
					setOuputDirectory(args[++i]);
				}
				else if (arg.equals("-L")) {
					setLibrayName(args[++i]);
				}
				else if (arg.equals("-J")) {
					setJavaPrefix(args[++i]);
				}
				else if (!arg.startsWith("-"))
					setInputFile(arg);
				else {
					throw new ArgumentException("Unrecognised argument: " + arg);
				}
			}
			catch (Exception e) {
				e.printStackTrace();
				throw new ArgumentException("Error processing argument: " + arg);
			}
		}
		checkOptions();
	}
	
	public void checkOptions() throws ArgumentException {
		if (inputFile == null)
			throw new ArgumentException("Input file not specified");
	}

	public File getOutputDirectory() {
		if (outputDirectory == null)
			outputDirectory = new File(".");
		return outputDirectory;
	}

	public List<File> getIncludeDirs() {
		return includeDirs;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = new File(inputFile);
		
		// Add source file directory to include dirs
		File parentFile = this.inputFile.getParentFile();
		if (parentFile != null)
			includeDirs.add(parentFile);
	}

	public File getInputFile() {
		return inputFile;
	}

	public void setOuputDirectory(String path) {
		outputDirectory = new File(path);
	}

	public void setJavaPrefix(String javaPrefix) {
		this.javaPrefix = javaPrefix;
	}
	
	public String getJavaPrefix() {
		if (javaPrefix == null)
			javaPrefix = "";
		return javaPrefix;
	}
	
	public String getLibrayName() {
//		if (libraryName == null) {
//			File file = getInputFile();
//			libraryName = file.getName().substring(0, file.getName().indexOf("."));
//			if (libraryName.length() == 0)
//				libraryName = "unnamed";
//		}
		return libraryName;
	}

	public void setLibrayName(String libraryName) {
		this.libraryName = libraryName;
	}

	/**
	 * Convenience method, returns getOutputDirectory().getPath()
	 * @return
	 */
	public String getOutputPath() {
		return getOutputDirectory().getPath() + "/";
	}

	public String getArraySuffix() {
		if (arraySuffix == null)
			arraySuffix = "_Array";
		return arraySuffix;
	}

	public String getTypePrefix() {
		if (typePrefix == null)
			typePrefix = "";
		return typePrefix;
	}

	public String getPrxSuffix() {
		if (implSuffix == null)
			implSuffix = "Prx";
		return implSuffix;
	}

	public String getPointerSuffix() {
		if (pointerSuffix == null)
			pointerSuffix = "_Ptr";
		return pointerSuffix;
	}

}
