In order to allow the use of Ada libraries with Java GUIs, a JNI (Java Native Interface) binding must be created which allows the JVM to call the non-java code, and vice versa.

Working with the JNI is complicated, and maintenance to the interface is time consuming.

A more intuitive approach is to make Java objects directly visible to the native code, and vice versa. Javada allows this with Ada by providing automatically generated 'proxy' objects, which present the correct interface to the developer while hiding the complexity of the JNI and performing memory management behind the scenes.

Javada automatically generates bindings which allow Ada and Java to talk to each other. The bindings are implemented using the object oriented features of Ada to create a 'mirror' class hierarchy in Ada and Java. The model for the bindings is based loosely on the CORBA proxy/skeleton model, and hides the interface from both the Ada and Java implementations (for example, creating new classes, interrogating fields, etc).

Instead of starting from either Ada or Java, the generator uses CORBA IDL (Interface Definition Language) as a platform-neutral description of the interface. The tool uses the IDL parser from eclipsecorba.sf.net to generate a model of the IDL definition, which is then used as the input to JET (Java Emitter Templates).

JET is a templating system which builds a Java class from a template containing mixed text and code, in the style of JSP (Java Server Pages), or ASP (Active Server Pages). The templates are easy to modify, if slightly difficult to read at first.

News

 * 2008-06-17 - AdaCore have released their version of an Ada/Java binding generator called GNAT-AJIS
 * 2009-02-02 - Fixed incompatibilities with latest versions of dependencies, re-licenced as LGPL for compatibility with Eclipse libraries.